# Ringneck - Component Project Model Dependency Mutation Analysis

Tim Storer

## Overview

Ringneck is a tool for assessing the extent of over and under-constraints in a
project model assembly.

The project repository is organised into sub-projects.

  * impl contains the core Ringneck tool, unit and integration tests.

  * experiment contains shared scripts and executables for experiments.

  * each sub-directory in experiments packages code and resources for the execution and analysis of a single experiment.

## Experiments

0001. Analysis of the maven eco-system sub-projects.

0002. Analysis of all projects reported by a query of the Maven Central Repository.

## Usage

All sub-projects are provided with an Ant build script, which uses Apache Ivy for dependency 
management.  At least one release of Ringneck must be built and installed in the local Ivy repository 
before use.  

    cd RINGNECK_HOME/impl
    ant deploy
    cd RINGNECK_HOME/experiment
    ant deploy

Experimental execution occurs in two phases (generation and analysis).  For example, to execute 
experiment 0001: 

    cd RINGNECK_HOME/experiments/0001
    ant run
    ant results
