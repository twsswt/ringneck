package uk.ac.glasgow.ringneck.test.unit;

import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.Collection;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import uk.ac.glasgow.ringneck.scm.GitScmDeployer;

@RunWith(Parameterized.class)
public class GitScmDeployerTest {

	@BeforeClass
	public static void beforeClass (){
		configure("src/test/resource/log4j.properties");
	}
		
	@Parameterized.Parameters
	public static Collection<Object[]> data (){
		return asList(
			new Object[][]{
				{"https://github.com/junit-team/junit", "r4.12", new File("target/workspaces/junit-junit-4.12")}
			});
		
		/* {
			{}
			//new Object[] {"git://github.com/ModeShape/modeshape.git", new File("target/workspaces/")},
			//new Object[] {"git://github.com/Yubico/java-u2flib-server.git", new File("target/workspaces/java-u2flib-server")}
		});*/
	}
	
	private final String repositoryURLString;
	private final String scmTag;
	
	private final File workingCopyPath;
	
	public GitScmDeployerTest (String repositoryURLString, String scmTag, File workingCopyPath){
		this.repositoryURLString = repositoryURLString;
		this.scmTag = scmTag;
		
		this.workingCopyPath = workingCopyPath;
	}
	
	private GitScmDeployer gitScmDeployer;
		
	@Before
	public void setUp() throws Exception {
		gitScmDeployer =
			new GitScmDeployer(repositoryURLString, scmTag);	
	}

	@Test
	public void test() throws Exception {
		gitScmDeployer.deployTo(workingCopyPath);
			
		File pomFile = new File (workingCopyPath.getAbsolutePath() + "/pom.xml");
		
		assertThat(pomFile.exists(), is(true));
		
	}

}
