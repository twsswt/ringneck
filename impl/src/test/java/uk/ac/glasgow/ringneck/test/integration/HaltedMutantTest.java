package uk.ac.glasgow.ringneck.test.integration;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.log4j.PropertyConfigurator;
import org.apache.maven.model.Dependency;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapper;

/**
 * This test confirms that the use of org.mockito:mockito-core:1.10.18 of the results in a failed compile build result.
 * @author tws
 *
 */
public class HaltedMutantTest {

	private MavenBuildRunnerProcessWrapper wrapper;

	@BeforeClass 
	public static void setupBeforeClass (){
		PropertyConfigurator.configure("src/test/resource/log4j.properties");
	}
	
	@Before
	public void setUp() throws Exception {
		
		File projectDir = new File("target/workspaces/org.apache/maven/maven-aether-provider");
		
		File pomFile = new File(projectDir.getPath()+"/pom.xml");
		
		ModelHelper modelHelper = ModelHelper.readModelFromFile(pomFile);
		
		Dependency toAlter = modelHelper.getMatchingConcreteDependencyByGroupAndArtifact("org.mockito", "mockito-core");
		
		toAlter.setVersion("[1.10.18]");
		
		modelHelper.writeModelToPOM(pomFile);
		
		wrapper = new MavenBuildRunnerProcessWrapper (projectDir, System.out, System.err);
				
	}
	
	@Test
	public void test() throws Exception {
		BuildResult actual = wrapper.buildProject();
		
		BuildResult expected = BuildResult.values()[2];
		
		assertEquals(expected, actual);
	}


}
