package uk.ac.glasgow.ringneck.test.unit;

import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.*;

import java.io.File;
import java.util.Set;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.ringneck.DependencyMutation;
import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.RingneckException;

public class ModelHelperDependencyVersionTest {

	private ModelHelper modelHelper;
	
	@Before
	public void setUp() throws Exception {
		configure("src/test/resource/log4j.properties");		

		File pomFile = new File("src/test/resource/org.apache-apache-maven-3.3.3.pom");
		
		modelHelper = ModelHelper.readModelFromFile(pomFile);
		
	}
	
	@Test
	public void testMatchDependency (){
		Dependency dependency =
			modelHelper.getMatchingConcreteDependencyByGroupAndArtifact("org.apache.maven", "maven-embedder");	
		
		assertNotNull(dependency);
	}

	@Test
	public void testGetDependencyVersion() {
		
		Dependency dependency =
			modelHelper.getMatchingConcreteDependencyByGroupAndArtifact("org.apache.maven", "maven-embedder");		
		
		String version = modelHelper.getDependencyVersion(dependency);
		
		assertEquals("3.3.3", version);
	}
	
	@Test
	public void testGetDependencyVersionExternalArtifact () {
		Dependency dependency =
			modelHelper.getMatchingConcreteDependencyByGroupAndArtifact("org.apache.maven.wagon", "wagon-file");		
		
		String version = modelHelper.getDependencyVersion(dependency);
		
		assertEquals("2.9", version);	
	}
	
	@Test
	public void testFilterDependencies () throws RingneckException {
		Set<DependencyMutation> mutantPopulation = 
			modelHelper.getMutantPopulation();
		
		Model mutantModel = mutantPopulation.iterator().next().mutantModel;
		
		Set<Dependency> mutatedDependencies = 
			modelHelper.getDependencyDifferenceSet(mutantModel);
				
		assertEquals(1, mutatedDependencies.size());
	}

}
