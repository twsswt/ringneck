package uk.ac.glasgow.ringneck.test.unit;

import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.assertEquals;
import static uk.ac.glasgow.ringneck.ModelHelper.readModelFromFile;

import java.io.File;
import java.util.List;

import org.apache.maven.model.Model;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import uk.ac.glasgow.ringneck.MutantPopulation;

@RunWith(Parameterized.class)
public class MutantPopulationTest {
	
	{
		configure("src/test/resource/log4j.properties");
	}
	
	@Parameterized.Parameters
	public static List<? extends Object[]> data () throws Exception {
		
		Object[][] result = {
			//pom model file path, number of mutants
			{"src/test/resource/junit-4.12.pom.xml", 4}
		};		
		return asList(result);
	}	

	private Model origin;
	private Integer numberOfMutants;

	public MutantPopulationTest (String modelFilePath, Integer numberOfMutants) throws Exception{
		this.origin = 
			readModelFromFile(new File(modelFilePath)).getModel();
		
		this.numberOfMutants = numberOfMutants;
	}
	
	private MutantPopulation mutantPopulation;
	
	@Before
	public void setUp() throws Exception {		
				
		mutantPopulation = new MutantPopulation(origin);
	}

	@Test
	public void test() throws Exception {
		
		Integer expected = numberOfMutants;
		Integer actual = mutantPopulation.size();
		
		String artifactId = origin.getArtifactId();
		String groupId = origin.getGroupId();
		
		String message = 
			String.format(
				"Incorrect number of mutants for project [%s:%s].",
				groupId, artifactId);
		
		assertEquals(message, expected, actual);
	}
}
