package uk.ac.glasgow.ringneck.test.unit;

import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.maven.model.Model;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;

public class ModelHelperIOTest {

	private static final File pomFile = new File("src/test/resource/forge-parent-2.12.3.Final.pom");

	private static final File pomOutFile = new File("model-io.test.xml");
	
	@After
	@Before
	public void setup () {
		configure("src/test/resource/log4j.properties");		

		assertTrue("Can't delete temporary test file", !pomOutFile.exists() || pomOutFile.delete() );
	}

	@Test
	public void testReadWriteModelFromFile() throws IOException, XmlPullParserException {
		
		assertTrue("Test set model file not present.", pomFile.exists());
		
		Model model = ModelHelper.readModelFromFile(pomFile ).getModel();
		
		assertNotNull("Couldn't read test set model file.", model);
		
		String actual = model.getArtifactId();
		String expected = "forge-parent";
		
		assertEquals("Incorrect artifact ID.", expected, actual);
		
		assertFalse("Test output file shouldn't exist yet.", pomOutFile.exists());
	
		new ModelHelper(model).writeModelToPOM(pomOutFile);
		
		assertTrue("Couldn't write pom.", pomOutFile.exists());
		
		Model outputModel = ModelHelper.readModelFromFile(pomOutFile).getModel();
		
		assertNotNull("Couldn't read output model file.", outputModel);		
	}
	
}
