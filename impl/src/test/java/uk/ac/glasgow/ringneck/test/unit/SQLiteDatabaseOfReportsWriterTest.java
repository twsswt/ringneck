package uk.ac.glasgow.ringneck.test.unit;

import static org.apache.log4j.PropertyConfigurator.configure;
import static uk.ac.glasgow.ringneck.ProjectResult.NO_DEPENDENCIES;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.RingneckReport;
import uk.ac.glasgow.ringneck.SQLiteDatabaseOfReportsWriter;

public class SQLiteDatabaseOfReportsWriterTest {
	
	private File databaseFile;
	
	private SQLiteDatabaseOfReportsWriter writer;
	
	private RingneckReport report1;
	private RingneckReport report2;


	@Before
	public void setUp() throws Exception {
		
		configure("src/test/resource/log4j.properties");
		
		databaseFile = new File("bin/ringneck.db");
		
		writer = new SQLiteDatabaseOfReportsWriter(databaseFile);
		
		String pomFilePath1 = "src/test/resource/org.apache-apache-maven-3.3.3.pom";
		String pomFilePath2 = "src/test/resource/org.apache-maven-aether-provider-3.3.3.pom";
		
		ModelHelper model1 = ModelHelper.readModelFromFile(new File(pomFilePath1));
		ModelHelper model2 = ModelHelper.readModelFromFile(new File(pomFilePath2));
		
		report1 = new RingneckReport(model1, NO_DEPENDENCIES);
		report2 = new RingneckReport(model2, NO_DEPENDENCIES);

	}

	@Test
	public void test() {
		writer.processExperiment(report1);
		writer.processExperiment(report2);
		
		//TODO Assert that DB contains two reports.
		
	}
	
	@After 
	public void tearDown (){
		databaseFile.delete();
	}

}
