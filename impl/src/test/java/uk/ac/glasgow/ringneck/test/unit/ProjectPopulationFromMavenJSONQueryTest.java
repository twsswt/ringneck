package uk.ac.glasgow.ringneck.test.unit;

import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.ProjectPopulationFromMavenJSONQuery;

public class ProjectPopulationFromMavenJSONQueryTest {
		
	private ProjectPopulationFromMavenJSONQuery projectPopulation;

	@Before
	public void setUp() throws Exception {
		
		configure("src/test/resource/log4j.properties");

		
		// References a cache of the following query on maven central.
		// http://search.maven.org/solrsearch/select?q=p:"pom"&rows=20&wt=json		
		
		File jSONQueryResultFile = new File("src/test/resource/test-unit.json");
		
		projectPopulation =
			new ProjectPopulationFromMavenJSONQuery(jSONQueryResultFile);
	}


	@Test
	public void testPopulationSize() {
		Integer expected = 20;
		Integer actual = projectPopulation.size();

		String message = 
			"Expected number of projects in query.";	
		
		assertEquals(message, expected, actual);	
	}
	
	@Test
	public void testFirstProject() throws Exception {
		ModelHelper firstModel = projectPopulation.iterator().next();
				
		assertNotNull(firstModel);
	}

}
