package uk.ac.glasgow.ringneck.test.unit;

import static org.apache.log4j.PropertyConfigurator.configure;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static uk.ac.glasgow.ringneck.ModelHelper.readModelFromFile;
import static uk.ac.glasgow.ringneck.builder.BuildResult.FAILED_COMPILE;
import static uk.ac.glasgow.ringneck.builder.BuildResult.SUCCESS;

import java.io.File;
import java.net.MalformedURLException;

import org.easymock.EasyMockRule;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.ProjectResult;
import uk.ac.glasgow.ringneck.RingneckException;
import uk.ac.glasgow.ringneck.RingneckMethod;
import uk.ac.glasgow.ringneck.RingneckMethodBuilder;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapper;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapperFactory;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerException;

public class RingneckMethodTest extends EasyMockSupport{
	
	@BeforeClass
	public static void beforeClass (){
		configure("src/test/resource/log4j.properties");
	}
	
	@Rule
	public EasyMockRule rule = new EasyMockRule(this);

	@Mock
	private ScmDeployer scmDeployer;
		
	@Mock
	private MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessFactory;
	
	@Mock
	private MavenBuildRunnerProcessWrapper mavenBuildRunnerProcessWrapper;
	
	
	private File projectWorkingDirectory;
	
	private final String workingDirectoryBasePath = "target/workspaces/junit-junit-4.12";
	
	private final String testModelFilePath = "src/test/resource/junit-4.12.pom.xml";
	
	private RingneckMethodBuilder ringneckMethodBuilder;
			
	@Before
	public void setUp () throws Exception {
		
		// Ensure the reference model is present in the workspace.
		ModelHelper modelHelper = readModelFromFile(new File(testModelFilePath));
		
		projectWorkingDirectory = modelHelper.getWorkingDirectory(new File(workingDirectoryBasePath));
		projectWorkingDirectory.mkdirs();
		File pomFile = new File(projectWorkingDirectory.getPath()+"/pom.xml");
		
		modelHelper.writeModelToPOM(pomFile);
		
		ringneckMethodBuilder =
			new RingneckMethodBuilder ()
				.setScmDeployer(scmDeployer)
				.setWorkingDirectory(projectWorkingDirectory)
				.setProjectDirectory(projectWorkingDirectory)
				.setMavenBuildRunnerProcessWrapperFactory(mavenBuildRunnerProcessFactory)
				.setDefaultBuildTimeout(1000l * 60l * 10l)
				.setMutantTimeoutFactor( 2);
	}
	
	@Test
	public void test() throws MalformedURLException, RingneckException, ScmDeployerException {

		// Evaluate the reference model.
		scmDeployer.deployTo(projectWorkingDirectory);
		expect(mavenBuildRunnerProcessFactory.createBuildProcessWrapper(1000l * 60l * 10l))
			.andReturn(mavenBuildRunnerProcessWrapper);
		expect(mavenBuildRunnerProcessWrapper.buildProject()).andReturn(SUCCESS);
		expect(mavenBuildRunnerProcessWrapper.getDuration()).andReturn(30 * 1000l);

		// Evaluate each variant.
		scmDeployer.deployTo(projectWorkingDirectory);
		expect(mavenBuildRunnerProcessFactory.createBuildProcessWrapper(30 * 1000 * 2l))
			.andReturn(mavenBuildRunnerProcessWrapper);
		expect(mavenBuildRunnerProcessWrapper.buildProject())
			.andReturn(FAILED_COMPILE);

		scmDeployer.deployTo(projectWorkingDirectory);
		expect(mavenBuildRunnerProcessFactory.createBuildProcessWrapper(30 * 1000 * 2l))
			.andReturn(mavenBuildRunnerProcessWrapper);
		expect(mavenBuildRunnerProcessWrapper.buildProject())
			.andReturn(FAILED_COMPILE);

		scmDeployer.deployTo(projectWorkingDirectory);
		expect(mavenBuildRunnerProcessFactory.createBuildProcessWrapper(30 * 1000 * 2l))
			.andReturn(mavenBuildRunnerProcessWrapper);
		expect(mavenBuildRunnerProcessWrapper.buildProject())
			.andReturn(FAILED_COMPILE);

		scmDeployer.deployTo(projectWorkingDirectory);
		expect(mavenBuildRunnerProcessFactory.createBuildProcessWrapper(30 * 1000 * 2l))
			.andReturn(mavenBuildRunnerProcessWrapper);
		expect(mavenBuildRunnerProcessWrapper.buildProject())
			.andReturn(FAILED_COMPILE);

		replayAll();
		RingneckMethod ringneckMethod = ringneckMethodBuilder.build();
		ProjectResult projectResult = ringneckMethod.run();
		assertEquals(ProjectResult.ANALYSIS_COMPLETED, projectResult);
		verifyAll();	
	}

}
