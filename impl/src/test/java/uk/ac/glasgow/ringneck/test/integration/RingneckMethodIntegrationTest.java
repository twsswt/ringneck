package uk.ac.glasgow.ringneck.test.integration;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;
import static uk.ac.glasgow.ringneck.ModelHelper.readModelFromFile;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.apache.maven.model.Dependency;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import uk.ac.glasgow.ringneck.DependencyMutation;
import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.ProjectResult;
import uk.ac.glasgow.ringneck.RingneckException;
import uk.ac.glasgow.ringneck.RingneckMethod;
import uk.ac.glasgow.ringneck.RingneckMethodBuilder;
import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapperFactory;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerFactory;

@RunWith(Parameterized.class)
public class RingneckMethodIntegrationTest {
	
	@BeforeClass
	public static void beforeClass (){
		configure("src/test/resource/log4j.properties");
	}
		
	@Parameterized.Parameters
	public static List<? extends Object[]> data () throws Exception {
		
		Object[][] result = {
			{"src/test/resource/junit-4.12.pom.xml"}
		};		
		return asList(result);
		
	}

	private ModelHelper modelHelper;
	
	public RingneckMethodIntegrationTest (String modelFilePath) throws Exception {
		this.modelHelper = 
			readModelFromFile(new File(modelFilePath));
	}
	
	private static final String workingDirName = "target/workspaces";
	private static final String buildReportsDir = "reports/ringneck";
	
	private RingneckMethod ringneckMethod;
			
	@Before
	public void setUp () throws Exception {

		File projectWorkingDirectory = 
			modelHelper.getWorkingDirectory(new File(workingDirName)); 
		
		File buildOutputFile = 
			modelHelper.getBuildReportsFile(new File(buildReportsDir));
		
		ScmDeployerFactory scmDeployerFactory = 
				new ScmDeployerFactory();
						
		ScmDeployer scmDeployer = 
			scmDeployerFactory.createScmDeployer(modelHelper);
		
		FileOutputStream fileOutputStream = new FileOutputStream(buildOutputFile, true);

		MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessFactory =
			new MavenBuildRunnerProcessWrapperFactory(projectWorkingDirectory, fileOutputStream);
		
		RingneckMethodBuilder ringneckMethodBuilder =
			new RingneckMethodBuilder ()
				.setScmDeployer(scmDeployer)
				.setWorkingDirectory(projectWorkingDirectory)
				.setProjectDirectory(projectWorkingDirectory)
				.setMavenBuildRunnerProcessWrapperFactory(mavenBuildRunnerProcessFactory)
				.setDefaultBuildTimeout(1000l * 60l * 10l)
				.setMutantTimeoutFactor( 2);
		
		
		ringneckMethod = ringneckMethodBuilder.build();
		
	}
	
	@Test
	public void test() throws MalformedURLException {
		
		try {
			ProjectResult projectResult = ringneckMethod.run();
			assertEquals("", ProjectResult.ANALYSIS_COMPLETED, projectResult);
			
			Map<DependencyMutation,BuildResult> mutantResults = ringneckMethod.getMutantResults();
									
			for (Map.Entry<DependencyMutation,BuildResult> buildResult: mutantResults.entrySet()){
				BuildResult actual = 
					buildResult.getValue();
				
				List<Dependency> dependencies = 
					buildResult.getKey().mutantModel.getDependencies();
								
				String message =
					format(
						"No mutants expected to pass, but mutated dependencies [%s] of model [%s] passed.",
						dependencies, modelHelper.getProjectName());
				
				BuildResult notExpected = BuildResult.SUCCESS;
				
				assertNotSame(message, notExpected, actual);
			}
						
		} catch (RingneckException e){
			e.printStackTrace();
			fail();
		}
	}

}
