package uk.ac.glasgow.ringneck.test.unit;

import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerException;
import uk.ac.glasgow.ringneck.scm.ScmDeployerFactory;

public class ScmDeployerFactoryTest {
	
	private static final String pomFilePath = 
		"src/test/resource/forge-parent-2.12.3.Final.pom";

	private ModelHelper modelHelper;
	
	private ScmDeployerFactory scmDeployerFactory;

	private ScmDeployer scmDeployer;
	
	
	@Before
	public void setUp() throws Exception {
		configure("src/test/resource/log4j.properties");
		
		File pomFile = new File(pomFilePath);
		modelHelper = ModelHelper.readModelFromFile(pomFile);
		
		scmDeployerFactory = new ScmDeployerFactory ();
		
	}

	@Test
	public void testDetermineSCMConnectionStringTypeModel() throws Exception {
		
		String expected = "git";
				
		String formattedConnectionString =
			modelHelper.getScmConnectionString();
		
		String actual = 
			ScmDeployerFactory.determineSCMConnectionStringType(formattedConnectionString);
		
		assertEquals("Incorrect SCM connection string type.", expected, actual);

	}
	
	@Test
	public void testScmConnectionStringInDeployer () throws ScmDeployerException{

		String expected = "git://github.com/forge/core.git";
		
		scmDeployer = scmDeployerFactory.createScmDeployer(modelHelper);
		
		String actual = scmDeployer.getScmConnectionString();
		
		assertEquals("Incorrect format for connection string.", expected, actual);

	}
	
	@Test
	public void testScmTagInDeployer () throws ScmDeployerException{

		String expected = "2.12.3.Final";
		
		scmDeployer = scmDeployerFactory.createScmDeployer(modelHelper);
		
		String actual = scmDeployer.getScmTag();
		
		assertEquals("Incorrect format for tag string.", expected, actual);

	}

}
