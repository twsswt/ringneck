package uk.ac.glasgow.ringneck.test.unit;

import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.*;
import static uk.ac.glasgow.ringneck.ModelHelper.readModelFromFile;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapper;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerFactory;

@RunWith(Parameterized.class)
public class MavenBuildRunnerTest {
	
	private static final Logger logger = Logger.getLogger(MavenBuildRunnerTest.class);
	
	private static final String WORKING_DIR_BASE_NAME =
		"target/workspaces/";
		
	
	@BeforeClass
	public static void beforeClass(){
		configure("src/test/resource/log4j.properties");
	}
	
	@Parameters
	public static List<Object[]> data () throws Exception {
		Object[][] result = {
			{"src/test/resource/junit-4.12.pom.xml"}
		};		
		return asList(result);
	}
	
	private ModelHelper modelHelper;
	
	private File workingDirBase;
	
	public MavenBuildRunnerTest (String modelFilePath) throws Exception {
		this.modelHelper = 
			readModelFromFile(new File(modelFilePath));
		
		workingDirBase = new File(WORKING_DIR_BASE_NAME);
		
	}
	
	private MavenBuildRunnerProcessWrapper wrapper;
	
	@Before
	public void setUp() throws Exception {
						
		logger.debug("Starting test ["+MavenBuildRunnerTest.class.getSimpleName()+"].");
							
		File workingDir = modelHelper.getWorkingDirectory(workingDirBase);
		
		ScmDeployerFactory scmDeployerFactory =
			new ScmDeployerFactory ();
		
		ScmDeployer scmDeployer = scmDeployerFactory.createScmDeployer(modelHelper);
		
		scmDeployer.deployTo(workingDir);
		
		wrapper = new MavenBuildRunnerProcessWrapper (workingDir, System.out, System.err);
	}
	
	@Test
	public void testBuildProject() throws Exception {
		BuildResult actual = wrapper.buildProject();

		String message = "Default build should pass.";

		BuildResult expected = BuildResult.SUCCESS;
						
		assertEquals(message, expected, actual);			
	}

}
