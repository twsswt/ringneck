package uk.ac.glasgow.ringneck.test.integration;

import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static org.junit.Assert.assertEquals;
import static uk.ac.glasgow.ringneck.ModelHelper.readModelFromFile;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.ProjectResult;
import uk.ac.glasgow.ringneck.RingneckMultiMethod;
import uk.ac.glasgow.ringneck.RingneckReport;
import uk.ac.glasgow.ringneck.builder.BuildResult;

public class RingneckMultiMethodIntegrationTest {
	
	private RingneckMultiMethod ringneckMultiMethod;
	
	@Before
	public void setUp() throws Exception {		
		configure("src/test/resource/log4j.properties");
		
		ModelHelper modelHelper = readModelFromFile(
			new File("src/test/resource/junit-4.12.pom.xml"));
		
		List<ModelHelper> projectPopulation = asList(modelHelper);
		
		File workingCopyDirectory = 
			new File("target/workspaces");
		
		File buildReportsFile = 
			new File("reports/ringneck/junit-junit-4.12/build-output.log"); 

		workingCopyDirectory.mkdirs();
		buildReportsFile.mkdirs();
				
		ringneckMultiMethod = 
			new RingneckMultiMethod(
				workingCopyDirectory,
				buildReportsFile,
				projectPopulation,
				1000l * 60l * 10l, 10);
	}

	@Test
	public void testRun() throws Exception {

		ringneckMultiMethod.run();
		
		RingneckReport report = ringneckMultiMethod.getNextRingneckReport();
		assertEquals (ProjectResult.ANALYSIS_COMPLETED, report.projectResult);			
		for (BuildResult buildResult : report.mutantResults.values())
			assertEquals("", BuildResult.FAILED_COMPILE, buildResult);
	}	

}
