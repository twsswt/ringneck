package uk.ac.glasgow.ringneck.builder;

import java.io.File;
import java.io.OutputStream;

public class MavenBuildRunnerProcessWrapperFactory {
		
	private File projectDirectory;

	private OutputStream outputStream;
	
	public MavenBuildRunnerProcessWrapperFactory (File projectDirectory, OutputStream outputStream) {
		this.projectDirectory = projectDirectory;
		this.outputStream = outputStream;
	}

	public MavenBuildRunnerProcessWrapper createBuildProcessWrapper(Long timeout) {
		return 
			new MavenBuildRunnerProcessWrapper(projectDirectory, timeout, outputStream, outputStream);		
	}
}
