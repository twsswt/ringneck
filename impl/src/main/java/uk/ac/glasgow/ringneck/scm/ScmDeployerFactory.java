package uk.ac.glasgow.ringneck.scm;

import static java.lang.String.format;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.RingneckException;

public class ScmDeployerFactory {
	
	public ScmDeployer createScmDeployer(ModelHelper modelHelper) throws ScmDeployerException {
		
		String projectName = modelHelper.getProjectName();
				
		String scmConnectionString;
		String scmTag;
		try {
			scmConnectionString = modelHelper.getScmConnectionString();
			scmTag = modelHelper.getScmTag();
			
		} catch (RingneckException e) {
			throw new ScmDeployerException(
				format("Whilst retrieving Scm connection string for project [%s].", 
					projectName,e));
		}
		
		if (scmConnectionString == null){
			String message = 
				format("Project [%s] has no SCM connection string defined, skipping.", 
					projectName);
				
			throw new ScmDeployerException(message);
		}
		
		
		if (scmConnectionString.contains("$")){
			String message = 
				format(
					"Couldn't evaluate all variables in connection string [%s] from project [%s].",
					scmConnectionString,
					projectName
				);
			
			throw new ScmDeployerException(message);	
		}
		
		return createScmDeployer(scmConnectionString, scmTag);
	}
	
	public ScmDeployer createScmDeployer(String scmConnectionString) throws ScmDeployerException {
		return createScmDeployer(scmConnectionString, null);
	}
		
	
	public ScmDeployer createScmDeployer(String scmConnectionString, String scmTag) throws ScmDeployerException {
		
		if (scmConnectionString.startsWith("scm:"))
			scmConnectionString = scmConnectionString.substring(4);
		
		String scmType = 
			determineSCMConnectionStringType(scmConnectionString);
				
		if (scmType == null){
			String template = "Unknown SCM type for formatted connection string [%s].";
			String message = format(template, scmConnectionString);
			
			throw new ScmDeployerException(message);
		
		} else if (scmType.equals("git")){
			scmConnectionString = cleanUpGitConnectionString(scmConnectionString);
			return new GitScmDeployer(scmConnectionString, scmTag);
		} else {
			String template = "Couldn't determine SCM type for connection [%s].";
			String message = format(template, scmConnectionString);
			
			throw new ScmDeployerException(message);			
		}
	}
		
	public static String determineSCMConnectionStringType(String connectionString) {
				
		String [] chunks = connectionString.split(":");
		
		if (chunks.length < 1) return null;	
		else if (chunks[0].startsWith("svn")) return "svn";
		else if (chunks[0].startsWith("git")) return "git";
		else if (chunks[0].startsWith("hg")) return "hg";
		else if (chunks[0].startsWith("cvs")) return "cvs";
		else if (chunks[0].startsWith("monotone")) return "monotone";
		
		else if (chunks[0].startsWith("http") || chunks[0].startsWith("scm")){
			if (chunks[1].contains("svn")) return "svn";
			else if (chunks[1].contains("git")) return "git";
			else if (chunks[1].contains("fossil")) return "fossil";
			else return null;
		} else if (	chunks[0].equals("") || chunks[0].equals("empty") ){
			return null;
		} else {
			return chunks[0];		
		}
	}
	
	private static String cleanUpGitConnectionString(String gitScmConnectionString) {
		
		if (
				gitScmConnectionString.startsWith("git:git://") || 
				gitScmConnectionString.startsWith("git:http://") ||
				gitScmConnectionString.startsWith("git:https://")
			)
			gitScmConnectionString = gitScmConnectionString.substring(4);
					
		gitScmConnectionString = gitScmConnectionString.replace("github.com:", "github.com/");
		
		gitScmConnectionString = gitScmConnectionString.replace("git@github", "github");
		
		if (gitScmConnectionString.startsWith("git:github.com"))
			gitScmConnectionString =
				gitScmConnectionString.replace("git:github.com", "git://github.com");
		
		return gitScmConnectionString;
	}
	

}
