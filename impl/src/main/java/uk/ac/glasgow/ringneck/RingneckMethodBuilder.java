package uk.ac.glasgow.ringneck;

import java.io.File;

import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapperFactory;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;

public class RingneckMethodBuilder {

	private File workingDirectory;
	private ScmDeployer scmDeployer;
	private File projectDirectory;
	
	private MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessWrapperFactory;
	
	private Long defaultBuildTimeout;
	private Integer mutantTimeoutFactor;

	public RingneckMethod build() {
		return new RingneckMethod(
			workingDirectory,
			scmDeployer,
			projectDirectory,
			mavenBuildRunnerProcessWrapperFactory,
			defaultBuildTimeout,
			mutantTimeoutFactor);
	}
	
	public RingneckMethodBuilder setScmDeployer(ScmDeployer scmDeployer) {
		this.scmDeployer = scmDeployer;
		return this;
	}

	public RingneckMethodBuilder setWorkingDirectory(File workingDirectory) {
		this.workingDirectory = workingDirectory;
		return this;
	}

	public RingneckMethodBuilder setMavenBuildRunnerProcessWrapperFactory(
		MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessWrapperFactory) {
		this.mavenBuildRunnerProcessWrapperFactory = mavenBuildRunnerProcessWrapperFactory;
		return this;
	}
	
	public RingneckMethodBuilder setProjectDirectory(File projectDir) {
		this.projectDirectory = projectDir;
		return this;
	}

	public RingneckMethodBuilder setDefaultBuildTimeout(Long defaultBuildTimeout) {
		this.defaultBuildTimeout = defaultBuildTimeout;
		return this;
	}

	public RingneckMethodBuilder setMutantTimeoutFactor(Integer mutantTimeoutFactor) {
		this.mutantTimeoutFactor = mutantTimeoutFactor;
		return this;
	}

}
