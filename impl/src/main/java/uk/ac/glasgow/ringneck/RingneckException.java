package uk.ac.glasgow.ringneck;

public class RingneckException extends Exception {

	/****/
	private static final long serialVersionUID = -2615864994026838907L;

	public RingneckException(String message) {
		super(message);
	}

	public RingneckException(Throwable throwable) {
		super(throwable);
	}

	public RingneckException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
