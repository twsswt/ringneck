package uk.ac.glasgow.ringneck;

import static java.lang.String.format;

import java.io.Serializable;
import java.util.Map;

import uk.ac.glasgow.ringneck.builder.BuildResult;

public class RingneckReport implements Serializable {
	
	/****/
	private static final long serialVersionUID = -6982672081716764285L;
	
	public final String artifactId;
	public final String groupId;
	public final String version;
	public final String scmConnectionString;
	
	public final ProjectResult projectResult;
	public final Map<DependencyMutation,BuildResult> mutantResults;
	
	public RingneckReport (
		ModelHelper modelHelper,
		ProjectResult projectResult) throws RingneckException {
				
		this(
			modelHelper.getProjectGroupId(),
			modelHelper.getModel().getArtifactId(),
			modelHelper.getModelVersion(),
			modelHelper.getScmConnectionString(),
			projectResult,
			null);
	}
	
	public RingneckReport (
		ModelHelper modelHelper,
		ProjectResult projectResult,
		Map<DependencyMutation, BuildResult> mutantReports) throws RingneckException{
				
		this(
			modelHelper.getProjectGroupId(),
			modelHelper.getModel().getArtifactId(),
			modelHelper.getModelVersion(),
			modelHelper.getScmConnectionString(),
			projectResult,
			mutantReports);
	}
	
	public RingneckReport (
		String groupId,
		String artifactId,
		String version,
		String scmConnectionString, 
		ProjectResult projectResult){
		
		this(
			groupId,
			artifactId,
			version,
			scmConnectionString,
			projectResult,
			null);
	}

	public RingneckReport(
		String groupId,
		String artifactId,
		String version,
		String scmConnectionString,
		ProjectResult projectResult,
		Map<DependencyMutation, BuildResult> mutantResults) {
		
		this.groupId = groupId;		
		this.artifactId = artifactId;
		this.version = version;
		
		this.scmConnectionString = scmConnectionString;
		this.projectResult = projectResult;
		this.mutantResults = mutantResults;
	}
	
	@Override
	public String toString (){
		
		Integer mutantResultsSize = mutantResults == null ? 0 : mutantResults.size();
		
		return format("[%s:%s:%s, %s, %s, %d mutants]",
			groupId,artifactId,version,scmConnectionString,projectResult, mutantResultsSize);
	}
}