package uk.ac.glasgow.ringneck;

import static java.lang.String.format;
import static java.util.stream.Collectors.toSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.log4j.Logger;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.Scm;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.version.Version;

public class ModelHelper {
	
	private static final Logger logger = Logger.getLogger(ModelHelper.class);

	private final Model model;
	
	public ModelHelper(Model model){
		this.model = model;
	}
	
	public Model getModel (){
		return model;
	}
		
	public void writeModelToPOM(File pomFile) throws IOException  {

		Writer writer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(pomFile), Charset.defaultCharset());
			MavenXpp3Writer xpp3Writer = new MavenXpp3Writer();
			xpp3Writer.write(writer, model);				
		} catch (IOException e) {
			throw e;
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e) {
					logger.warn(
						format("While closing stream to file [%s].", pomFile), e);
				}
		}
	}


	public String getModelVersion() throws RingneckException {
		String version = model.getVersion();
		
		if (version == null){
			try {
				
				ModelHelper parentModelHelper = getParentModelHelper();
				version = parentModelHelper.getModel().getVersion();
				
			} catch (
				ArtifactResolutionException | IOException | XmlPullParserException e) {

				String template = 
					"While retrieving parent model helper to obtain version for project with artifact id [%s].";
				
				String message = 
					format(template, model.getArtifactId());

				throw new RingneckException(message ,e);
			}

		}
		return version;
	}

	public static String getProjectDependencyName(Dependency dependency) {
		String artifactId = dependency.getArtifactId();
		String groupId = dependency.getGroupId();
		return groupId + ":" + artifactId;
	}

	public String getProjectName() {
		String artifactId = model.getArtifactId();
		
		String groupId = null;
		try {
			groupId = getProjectGroupId();
		} catch (RingneckException e) {
			logger.warn(format (
				"Couldn't retrieve group Id for project with artifactId [%s].",
				artifactId));
		}
		
		String version = null;

		try {
			version = this.getModelVersion();
		} catch (RingneckException e) {
			logger.warn(format (
				"Couldn't retrieve version for project with artifactId [%s].",
				artifactId),e);
		}
			
		return groupId + ":" + artifactId + ":" + version;
	}
	
	public String getProjectGroupId () throws RingneckException{
		String groupId = model.getGroupId();
		try {
			ModelHelper parent = getParentModelHelper();
			while (groupId == null){
				groupId = parent.getProjectGroupId();
				parent = parent.getParentModelHelper();
			}
		} catch (ArtifactResolutionException | IOException | XmlPullParserException e) {
			
			String template = 
				"While retrieving parent model helper to obtain group id for project with artifact id [%s].";
			String message = format(template, model.getArtifactId());
			
			throw new RingneckException(message,e);
		}
		return groupId;
	}

	public File getWorkingDirectory(File workingDirBase) {
		return new File (workingDirBase.getPath()+"/"+getStandardProjectDirectoryName());
	}
	
	public File getBuildReportsFile(File reportsDirectory){
		return new File(reportsDirectory.getPath()+"/"+getStandardProjectDirectoryName()+"/build-output.log");
	}
	
	private String getStandardProjectDirectoryName (){
		return model.getGroupId()+"-"+model.getArtifactId()+"-"+model.getVersion();
	}
	
	public String getScmConnectionString() throws RingneckException {		
		
		Scm scm = getScm();
		if (scm == null)
			return null;
		
		String rawScmConnectionString = 
			scm.getConnection();
		if (rawScmConnectionString == null)
			return null;
		
		String evaluatedConnectionString =
			evaluateVariables(rawScmConnectionString);			
				
		return evaluatedConnectionString;
						
	}

	public String getScmTag() throws RingneckException {

		Scm scm = getScm();
				
		if (scm != null)
			return evaluateVariables(scm.getTag());
		else
			return null;
	}
	
	private Scm getScm () throws RingneckException {
		Scm scm = model.getScm();
		if (scm != null) return scm;
		
		ModelHelper parentModelHelper;
		try {
			parentModelHelper = getParentModelHelper();
			if (parentModelHelper != null)
				return parentModelHelper.getScm();
			else
				return null;

		} catch (ArtifactResolutionException | IOException | XmlPullParserException e) {
			String template = 
				"While retrieving parent model helper to obtain Scm tag for project with artifact id [%s].";
			String message = 
				format(template, model.getArtifactId());
			throw new RingneckException (message, e);								
		}
					
	}

	public String getDependencyVersion(Dependency dependency) {
		
		String version = evaluateVariables(dependency.getVersion());
		
		if (version == null){
			
			String artifactId = dependency.getArtifactId();
			String groupId = dependency.getGroupId();
			
			try {
				ModelHelper parentModelHelper = getParentModelHelper();
				
				Dependency matchedDependency=
					parentModelHelper.getMatchingManagedDependencyByGroupAndArtifact(groupId, artifactId);
				
				version = evaluateVariables(matchedDependency.getVersion());
				
			} catch (ArtifactResolutionException | IOException |XmlPullParserException e) {
				String template = 
					"Couldn't retrieve parent model helper to obtain version for project [%s].";
				String message = 
					format(template, model.getArtifactId());
				logger.warn (message, e);								
				
			} 
		}
		
		return version;
	}
	
	public String evaluateVariables(String expression) {
		
		if (expression == null) return null;
		
		
		StringBuffer buffer = new StringBuffer(expression);
		
		Integer start = buffer.indexOf("${",0);
		Integer end  = buffer.indexOf("}", 1);
				
		while (start >= 0 && end > start){
		
			String key = buffer.substring(start+2, end);
			String value = findModelPropertyValue(key);
			
			if (value != null) buffer.replace(start, end+1, value);
			
			start = buffer.indexOf("${", start+1);
			end = buffer.indexOf("}", start);			
		}
	
		return buffer.toString();
	}

	private String findModelPropertyValue(String key) {
		
		Properties properties = getAllProjectProperties ();
		
		String value = properties.getProperty(key);
				
		if (value == null ){
			if (key.equals("project.artifactId") || key.equals("artifactId") || key.equals("pom.artifactId"))
				value = model.getArtifactId();
			else if (key.equalsIgnoreCase("project.groupId"))
				value = model.getGroupId();
			else if (key.equals("project.version")){
				try {
					value = getModelVersion ();
				} catch (RingneckException e) {
					String template = 
						"Couldn't retrieve parent model helper to obtain version for project [%s].";
					String message = 
						format(template, model.getArtifactId());
					logger.warn(message, e);								

				}
			}
		}
		return value;
	}

	private Properties getAllProjectProperties() {
		Properties allProperties = new Properties ();
		allProperties.putAll(model.getProperties());

		ModelHelper parentHelper = null;
		try {
			parentHelper = getParentModelHelper();
			if (parentHelper != null)
				allProperties.putAll(parentHelper.getAllProjectProperties());
		} catch (ArtifactResolutionException | IOException | XmlPullParserException e) {
			String template = 
				"While retrieving parent model helper to obtain properties for project [%s].";
			String message = 
				format(template, model.getArtifactId());
			logger.warn(message, e);								
		}			
		
		return allProperties;
		
	}

	
	public Set<Dependency> getDependencyDifferenceSet(Model mutant) {
		
		List<Dependency> candidates = model.getDependencies();
					
		
		Predicate<? super Dependency> predicate =
			dependency -> getMatchingDependency(candidates,
				candidate -> dependencyMatchesGroupArtifactAndVersion(
					candidate.getGroupId(),
					candidate.getArtifactId(),
					candidate.getVersion(),
					dependency)) == null;
		
		return 
			mutant.getDependencies()
			.stream()
			.filter(predicate)
			.collect(toSet());
	}

	public Dependency getMatchingConcreteDependencyByGroupAndArtifact(
		String groupId, String artifactId) {
		
		List<Dependency> candidates =
			model.getDependencies();
		
		return getMatchingDependency(
			candidates,
			dependency -> dependencyMatchesGroupAndArtifact(groupId, artifactId, dependency));
	}
	

	public Dependency getMatchingManagedDependencyByGroupAndArtifact(
		String groupId, String artifactId) {
		
		List<Dependency> candidates = 
			 model.getDependencyManagement().getDependencies();
				
		return getMatchingDependency(
			candidates,
			dependency -> dependencyMatchesGroupAndArtifact(groupId, artifactId, dependency));
	}
	
	private static Dependency getMatchingDependency(
		List<Dependency> dependencies,
		Predicate<? super Dependency> predicate) {
		
		for (Dependency dependency : dependencies){
			
			if (predicate.test(dependency))
				return dependency;
		}
		return null;
	}

	private static Boolean dependencyMatchesGroupAndArtifact(
		String groupId,
		String artifactId,
		Dependency dependency) {
		
		
		return 
			dependency.getGroupId().equals(groupId) &&
			dependency.getArtifactId().equals(artifactId);
	}
	
	private static Boolean dependencyMatchesGroupArtifactAndVersion(
		String groupId,
		String artifactId,
		String version,
		Dependency dependency) {
		
		return 
			dependency.getGroupId().equals(groupId) &&
			dependency.getArtifactId().equals(artifactId) &&
			((version == null && dependency.getVersion() == null) ||
				version != null && version.equals(dependency.getVersion()));

	}

	private ModelHelper getParentModelHelper()
		throws ArtifactResolutionException, 
		IOException,
		XmlPullParserException {
		if (model.getParent() == null) return null;
		
		Artifact parentArtifact = createParentPomArtifact();
		
		MavenRepositoryFacade mavenRepositoryFacade =
			MavenRepositoryFacade.getMavenRepositoryFacade();
		
		ArtifactResult result = 
			mavenRepositoryFacade.retrievePomArtifact(parentArtifact);
			
		File pomFile = result.getArtifact().getFile();
			
		return readModelFromFile(pomFile);
	}

	private Artifact createParentPomArtifact() {
		
		Parent parent = model.getParent();
		String parentGroupId = parent.getGroupId();
		String parentArtifactId = parent.getArtifactId();
		String parentVersion = parent.getVersion();
		
		return
			new DefaultArtifact(
				parentGroupId, parentArtifactId, "","pom", parentVersion);		
	}

	public Set<DependencyMutation> getMutantPopulation() throws RingneckException {
		return new MutantPopulation(model);
	}
	
	public Long getTheoreticalMutantPopulationSize() throws RingneckException {
		Long total = 1l;
		
		MavenRepositoryFacade mavenRepositoryFacade =
			MavenRepositoryFacade.getMavenRepositoryFacade();
				
		List<Dependency> dependencies = model.getDependencies();
		
		for (Dependency dependency : dependencies){
			List<Version> alternateVersions;
			try {
				alternateVersions =
					mavenRepositoryFacade.getAvailableDependencyVersions(dependency);
				total *= alternateVersions.size();
			} catch (VersionRangeResolutionException e) {
				throw new RingneckException(e);
			}
		}
		
		return total;
	}
	
	public static ModelHelper readModelFromFile(File pomFile) throws IOException, XmlPullParserException {
		
		Reader reader = null;
		try {
			reader = new InputStreamReader(new FileInputStream(pomFile), Charset.defaultCharset());
			MavenXpp3Reader mavenXpp3Reader = new MavenXpp3Reader();
			Model model = mavenXpp3Reader.read(reader);
			return new ModelHelper(model);

		} catch (IOException | XmlPullParserException e) {
			throw e;
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					logger.warn(
						format("While closing stream from file [%s].",pomFile),e);
				}
		}
	}

	
}
