package uk.ac.glasgow.ringneck;

import static java.lang.String.format;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

public class RingneckMultiMethod implements Runnable {
	
	class ModelEvaluationTask {

		public final ModelHelper modelHelper;
		public final RingneckMethod method;
		
		public ModelEvaluationTask(ModelHelper modelHelper, RingneckMethod method){
			this.method = method;
			this.modelHelper = modelHelper;
		}	
	}
		
	private final static Logger logger = Logger.getLogger(RingneckMethod.class);
				
	private Queue<ModelEvaluationTask> jobs;
	private BlockingQueue<RingneckReport> ringneckReports;
		
	public RingneckMultiMethod (
		File workingDirBase, 
		File buildReportsDir, 
		List<ModelHelper> referencePopulation, 
		Long defaultBuildTimeout,
		Integer timeoutFactor) 
		throws FileNotFoundException{
			
		if (!(workingDirBase.exists() || workingDirBase.mkdirs()))
			throw new FileNotFoundException(
				format("Couldn't create directory [%s].", workingDirBase));

		createMethods(referencePopulation, workingDirBase, buildReportsDir, defaultBuildTimeout, timeoutFactor);
	}

	private void createMethods(
		List<ModelHelper> referencePopulation, 
		File workingDirBase,
		File buildOutputFile,
		Long defaultBuildTimeout,
		Integer timeoutFactor) {
		
		jobs = new LinkedList<ModelEvaluationTask>();
		ringneckReports = new LinkedBlockingQueue<RingneckReport>();
		
		RingneckMethodFactory ringneckMethodFactory =
			new RingneckMethodFactory(workingDirBase, buildOutputFile, defaultBuildTimeout, timeoutFactor);
		
		for (ModelHelper modelHelper: referencePopulation){
			if (modelHelper == null)
				logger.info("Skipping a project with missing reference POM.");

			else {
				try {
					RingneckMethod ringneckMethod = ringneckMethodFactory.createRingneckMethod(modelHelper);
					jobs.offer(new ModelEvaluationTask(modelHelper, ringneckMethod));

				} catch (RingneckException e) {
					String projectName = modelHelper.getProjectName();
					logger.info(
						format(
							"Couldn't create experiment for project [%s].", projectName));
					
					try {
						RingneckReport report = 
							new RingneckReport(modelHelper, ProjectResult.UNDEPLOYABLE);
						ringneckReports.offer(report);

					} catch (RingneckException e1) {
						logger.warn(format("Couldn't log project [%s] details.", projectName), e1);
					}
				}
			}
		}
	}
		
	public void run() {
		logger.info(format("Starting experimental set of size [%d].", jobs.size()));

		while (jobs.peek() != null) {
			
			ModelEvaluationTask modelExperiment = jobs.poll();
			
			ModelHelper modelHelper = modelExperiment.modelHelper;
			
			String projectName = 
				modelHelper.getProjectName();
			
			RingneckMethod ringneckMethod = modelExperiment.method;

			logger.info(format("Starting analysis of project [%s].", projectName));
			
			RingneckReport report = null;
			try {
				ProjectResult result = ringneckMethod.run();
				logger.info(format("Finished analysis of project [%s].", projectName));
				report = new RingneckReport(modelHelper, result, ringneckMethod.getMutantResults());

			} catch (RingneckException e) {
				logger.warn(format("Couldn't evaluate project [%s].", projectName), e);
				try {
					report = new RingneckReport(modelHelper, ProjectResult.UNKNOWN);
				} catch (RingneckException re){
					logger.warn(format("Couldn't log project [%s] details.", projectName), e);
				}
			}
			
			ringneckReports.offer(report);
		}
	}
				
	public RingneckReport getNextRingneckReport() throws InterruptedException {
		return ringneckReports.take();
	}

	public boolean isRunning() {
		return !(jobs.isEmpty() && ringneckReports.isEmpty());
	}

}