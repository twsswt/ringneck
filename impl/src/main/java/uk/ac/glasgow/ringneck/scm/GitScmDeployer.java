package uk.ac.glasgow.ringneck.scm;

import static java.lang.String.format;
import static org.eclipse.jgit.api.Git.open;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CleanCommand;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;

public class GitScmDeployer implements ScmDeployer {
	
	private static final Logger logger = Logger.getLogger(GitScmDeployer.class);
	
	private Git git;
	
	private String scmConnectionString;
	private String scmTag;

	public GitScmDeployer(String scmConnectionString, String scmTag) {
		this.scmConnectionString = scmConnectionString;
		this.scmTag = scmTag;
	}
	
	@Override
	public String getScmConnectionString() {
		return scmConnectionString;
	}
	
	@Override
	public String getScmTag() {
		return scmTag;
	}

	@Override
	public String toString (){
		return format("type=Git,connectionString=%s,tag=%s", scmConnectionString, scmTag);
	};


	@Override
	public void deployTo(File workingCopyPath) throws ScmDeployerException {
		deployFromGit(workingCopyPath);
	}
	
	private void deployFromGit (File workingCopyPath) throws ScmDeployerException{
		if (!workingCopyPath.exists())
			cloneFromGit(workingCopyPath);
		else 
			checkoutFromGit(workingCopyPath);
		
		if (scmTag != null)
			checkoutTag();
	}

	private void cloneFromGit (File workingCopyPath) throws ScmDeployerException { 	
				
		CloneCommand cloneCommand = Git.cloneRepository();
			
		cloneCommand.setURI(scmConnectionString);
		
		cloneCommand.setDirectory(workingCopyPath);
		cloneCommand.setBranch("master");
		cloneCommand.setBare(false);
		cloneCommand.setRemote("origin");
		cloneCommand.setNoCheckout(false);
		
		try {
			git = cloneCommand.call();
		} catch (GitAPIException | JGitInternalException e) {
			String message = 
				format("While cloning Git repository from URL [%s]",
						scmConnectionString);
						
			throw new ScmDeployerException(message, e);
		}		
	}
	
	private void checkoutFromGit(File workingCopyPath) throws ScmDeployerException {

		logger.info(
			format("Reverting existing Git SCM deployment using checkout command in working copy path [%s]...",
				workingCopyPath));
		
		if (git == null)
			try {
				git = open(workingCopyPath);
			} catch (IOException e) {
				String message =
					format("While creating Git instance on existing working copy path [%s].",
						workingCopyPath);
				
				throw new ScmDeployerException(message, e);
			}	
		
					
		try {
			ResetCommand resetCommand = git.reset();
			resetCommand.setMode(ResetType.HARD);
			resetCommand.call();
			
			CleanCommand cleanCommand = git.clean();
			cleanCommand.call();
		} catch (GitAPIException e) {

			String message =
				format("While invoking reset on on existing working copy path [%s].", workingCopyPath);

			throw new ScmDeployerException(message, e);
		}
		logger.info(format("Finished reverting Git working copy at path [%s].", workingCopyPath));
	}
	
	private void checkoutTag() throws ScmDeployerException{
		CheckoutCommand checkoutCommand = git.checkout();
		checkoutCommand.setName(scmTag);
		try {
			checkoutCommand.call();
		} catch (GitAPIException e) {
			throw new ScmDeployerException(format("While checking out scm connection tag [%s]", scmTag), e);
		}
		
	}

}
