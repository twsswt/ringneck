package uk.ac.glasgow.ringneck;

import org.apache.maven.cli.logging.Slf4jLoggerManager;
import org.codehaus.plexus.ContainerConfiguration;
import org.codehaus.plexus.DefaultContainerConfiguration;
import org.codehaus.plexus.DefaultPlexusContainer;
import org.codehaus.plexus.PlexusConstants;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.PlexusContainerException;
import org.codehaus.plexus.classworlds.ClassWorld;

public class PlexusFacade {

	public static PlexusContainer createPlexusContainer() throws PlexusContainerException {
		
		ClassWorld classWorld = 
			new ClassWorld( "plexus.core", Thread.currentThread().getContextClassLoader() );
		
		ContainerConfiguration cc = new DefaultContainerConfiguration();
		cc.setClassWorld( classWorld );
		cc.setClassPathScanning( PlexusConstants.SCANNING_INDEX );
		cc.setAutoWiring( true );
		cc.setName( "maven" );
	
		DefaultPlexusContainer plexusContainer = new DefaultPlexusContainer(cc);
	
		// NOTE: To avoid inconsistencies, we'll use the TCCL exclusively for lookups
		plexusContainer.setLookupRealm( null );
		
		//ILoggerFactory slf4jLoggerFactory = 
		//		LoggerFactory.getILoggerFactory();
	    //Logger slf4jLogger = slf4jLoggerFactory.getLogger( MavenBuildRunner.class.getName() );
		 			
        Slf4jLoggerManager plexusLoggerManager = new Slf4jLoggerManager();
        plexusContainer.setLoggerManager(plexusLoggerManager);

	
		Thread.currentThread().setContextClassLoader( plexusContainer.getContainerRealm() );
	
		return plexusContainer;
	}
}
