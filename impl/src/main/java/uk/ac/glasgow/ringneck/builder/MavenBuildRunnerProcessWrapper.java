package uk.ac.glasgow.ringneck.builder;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.slf4j.LoggerFactory.getLogger;
import static uk.ac.glasgow.ringneck.builder.BuildResult.TIMED_OUT;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;

import uk.ac.glasgow.ringneck.RingneckException;

public class MavenBuildRunnerProcessWrapper {
	
	private final static Logger logger = 
		getLogger(MavenBuildRunnerProcessWrapper.class);
	
	private final File workingDir;
	private final Long timeout;
	
	private final OutputStream stdoutDestination;
	private final OutputStream errDestination;
	
	private Long duration;
	
	public MavenBuildRunnerProcessWrapper (File workingDir, OutputStream stdoutDestination, OutputStream errDestination){
		this(workingDir, null, stdoutDestination, errDestination);
	}

	public MavenBuildRunnerProcessWrapper (File workingDir, Long timeout, OutputStream stdoutDestination, OutputStream errDestination){
		this.workingDir = workingDir;
		this.timeout = timeout;
		
		this.stdoutDestination = stdoutDestination;
		this.errDestination = errDestination;
	}
	
	public BuildResult buildProject() throws RingneckException{		
				
		ProcessBuilder builder = createProcessBuilder();
		
		logger.info(
			format("Starting build process in directory [%s].", workingDir));
		
		Process process;
		try {
			process = builder.start();
		} catch (IOException e) {
			throw new RingneckException(
				format(
					"Couldn't initialise maven sub-process in directory [%s].",workingDir),
				e);
		}
			
		reDirectStream(process.getInputStream(), stdoutDestination);
		reDirectStream(process.getErrorStream(), errDestination);
		
		Long startTime = currentTimeMillis();
		Boolean timedOut = false;
		try {				
			if (timeout == null)
				process.waitFor();
			else
				timedOut = !process.waitFor(timeout, MILLISECONDS);
			
		} catch (InterruptedException e) {
			throw new RingneckException(
				format("While running maven process in directory [%s]", workingDir),e);
		}
		
		duration = currentTimeMillis() - startTime;
		
		process.destroyForcibly();
		
		int result = timedOut ? TIMED_OUT.ordinal() : process.exitValue();
		
		logger.info(format("Build process exited with value [%d].", result));		
		return BuildResult.values()[result];
	}
	
	public Long getDuration (){
		return duration;
	}

	private ProcessBuilder createProcessBuilder() {
		ProcessBuilder builder = new ProcessBuilder();
		
		String classPath = System.getProperty("java.class.path");
		String className = MavenBuildRunner.class.getName();
				
		String log4jConfigFileName = "src/main/resource/buildrunner-log4j.properties";
		
		File log4jConfigFile = 
			new File(log4jConfigFileName).getAbsoluteFile();
		
		builder.command("java","-cp", classPath, className, log4jConfigFile.getAbsolutePath());
		builder.directory(workingDir);
		return builder;
	}

	private void reDirectStream(final InputStream inputStream, final OutputStream outputStream) {		
		new Thread (){
			public void run (){
				try {
					int nextByte = inputStream.read();
					while (nextByte != -1){
						outputStream.write(nextByte);
						nextByte = inputStream.read();
					}
				} catch (IOException e) {
					logger.warn(
						format(
							"While re-directing IO stream for build runner working in directory [%s]", workingDir),e);
				}
			}
		}.start();
	}
	
}
