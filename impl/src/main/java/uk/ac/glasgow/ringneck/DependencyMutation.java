package uk.ac.glasgow.ringneck;

import java.io.Serializable;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;

public class DependencyMutation implements Serializable {

	/****/
	private static final long serialVersionUID = -7193059193280737654L;
	
	public final Model referenceModel;
	public final Model mutantModel;
	public final Dependency original;
	public final Dependency mutant;
	
	public DependencyMutation (Model referenceModel, Model mutantModel, Dependency original, Dependency mutant){
		this.referenceModel = referenceModel;
		this.mutantModel = mutantModel;
		this.original = original;
		this.mutant = mutant;
	}
	
	public Boolean mutantShouldHavePassed() {
		
		String originalVersion =
			new ModelHelper(referenceModel).getDependencyVersion(original);
		
		if (originalVersion == null) return true;


		try {
			VersionRange vr = VersionRange.createFromVersionSpec(originalVersion);
			return vr.containsVersion(new DefaultArtifactVersion(mutant.getVersion()));
		
		} catch (InvalidVersionSpecificationException e) {
			e.printStackTrace();
		}
		return null;		
	}

	
	public String toString (){
		String originalVersion = 
			new ModelHelper(referenceModel).getDependencyVersion(original);
		
		return
			original.getArtifactId()+":"+
			original.getGroupId()+":"+
			originalVersion+"->"+mutant.getVersion();
	}
}
