package uk.ac.glasgow.ringneck.scm;

public class ScmDeployerException extends Exception {

	public ScmDeployerException(String message) {
		super(message);
	}

	public ScmDeployerException(String message,	Exception e) {
		super(message, e);
	}

	/****/
	private static final long serialVersionUID = -1436427264953209798L;
	
	

}
