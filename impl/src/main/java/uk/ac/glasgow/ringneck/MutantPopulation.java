package uk.ac.glasgow.ringneck;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.version.Version;

public class MutantPopulation extends HashSet<DependencyMutation> {
	
	private static final Logger logger = Logger.getLogger(MutantPopulation.class);
	
	/****/
	private static final long serialVersionUID = -4482210463867503678L;
	
	private Model referenceModel;
	
	public MutantPopulation(Model referenceModel) throws RingneckException  {
		this.referenceModel = referenceModel;		
		try {
			createMutantPopulation();
		} catch (VersionRangeResolutionException e) {
			throw new RingneckException(e);
		}
	}
	
	
	private void createMutantPopulation () throws VersionRangeResolutionException{
				
		List<Dependency> dependencies = referenceModel.getDependencies();
		//List<Dependency> dependencies2 = 
		//referenceModel.getDependencyManagement().getDependencies();
		
		for (Dependency dependency: dependencies){
			List<DependencyMutation> mutants = getMutants(dependency);
			addAll(mutants);
		}		
	}
	
	private List<DependencyMutation> getMutants(Dependency dependency) throws VersionRangeResolutionException {
		
		String originalVersion = new ModelHelper(referenceModel).getDependencyVersion(dependency);
		String artifactId = dependency.getArtifactId();
		String groupId = dependency.getGroupId();
				
		String message = format(
			"Creating mutants for dependency [%s:%s:%s].", 
			groupId, artifactId, originalVersion);
		
		logger.info(message);
				
		List<DependencyMutation> results = new ArrayList<DependencyMutation>();
		
		MavenRepositoryFacade mavenRepositoryFacade =
			MavenRepositoryFacade.getMavenRepositoryFacade();
		
		List<Version> alternateVersions = 
			mavenRepositoryFacade.getAvailableDependencyVersions(dependency);
				
		for (Version alternateVersion: alternateVersions){
						
			String alternateVersionAsString = alternateVersion.toString();
				
			if (originalVersion == null || !originalVersion.equals(alternateVersionAsString)){
				
				String mutantVersion = format("[%s]",alternateVersionAsString);
				
				DependencyMutation mutantRelation = createMutantRelation(dependency, mutantVersion);
				
				if (mutantRelation != null)
					results.add(mutantRelation);
			}
		}
		
		return results;
		
	}

	private DependencyMutation createMutantRelation(Dependency dependency, String version) {
		Model clone = referenceModel.clone();
		List<Dependency> clonedDependencies =  clone.getDependencies();
		
		String artifactId = dependency.getArtifactId();
		
		for (Dependency clonedDependency: clonedDependencies)
			if (clonedDependency.getArtifactId().equals(artifactId)){
				
				clonedDependency.setVersion(version);
				return new DependencyMutation(referenceModel, clone, dependency, clonedDependency);
			}
		return null;
	}
}