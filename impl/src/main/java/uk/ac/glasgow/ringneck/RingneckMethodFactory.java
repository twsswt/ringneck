package uk.ac.glasgow.ringneck;

import static java.lang.String.format;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapperFactory;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerException;
import uk.ac.glasgow.ringneck.scm.ScmDeployerFactory;

public class RingneckMethodFactory {
	
	private final static Logger logger = 
		LoggerFactory.getLogger(RingneckMethodFactory.class);
	
	private final File workingDirBase;
	
	private final File buildOutputFile;
	
	private final Integer mutantTimeoutFactor;
	private final Long defaultBuildTimeout;
		
	public RingneckMethodFactory(File workingDirBase, File buildOutputFile, Long defaultBuildTimeout, Integer timeoutFactor) {
		this.workingDirBase = workingDirBase;
		this.buildOutputFile = buildOutputFile;
		this.mutantTimeoutFactor = timeoutFactor;
		this.defaultBuildTimeout = defaultBuildTimeout;
	}

	public RingneckMethod createRingneckMethod(ModelHelper modelHelper) throws RingneckException {
		
		String projectName = modelHelper.getProjectName();
		
		ScmDeployerFactory scmDeployerFactory = 
			new ScmDeployerFactory();
				
			ScmDeployer scmDeployer;
			try {
				scmDeployer = 
					scmDeployerFactory.createScmDeployer(modelHelper);
				logger.info(format("Created SCM deployer [%s].", scmDeployer));
			
			} catch (ScmDeployerException e) {
				logger.warn(format("Couldn't create deployer for project [%s], skipping.", projectName));
				throw new RingneckException(e);
			}
				
			File projectWorkingDirectory =  modelHelper.getWorkingDirectory(workingDirBase);
			
			FileOutputStream fileOutputStream;
			try {
				fileOutputStream = new FileOutputStream(buildOutputFile, true);
			} catch (FileNotFoundException e) {
				throw new RingneckException("Couldn't create output stream to file for logging maven build output.", e);
			}
			
			MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessFactory =
				new MavenBuildRunnerProcessWrapperFactory(projectWorkingDirectory, fileOutputStream);
			
			RingneckMethodBuilder ringneckMethodBuilder =
				new RingneckMethodBuilder ()
					.setScmDeployer(scmDeployer)
					.setWorkingDirectory(projectWorkingDirectory)
					.setProjectDirectory(projectWorkingDirectory)
					.setMavenBuildRunnerProcessWrapperFactory(mavenBuildRunnerProcessFactory)
					.setDefaultBuildTimeout(defaultBuildTimeout)
					.setMutantTimeoutFactor( mutantTimeoutFactor);
			
			
			return ringneckMethodBuilder.build();
		}
}
