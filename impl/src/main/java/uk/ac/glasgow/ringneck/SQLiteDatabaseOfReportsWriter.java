package uk.ac.glasgow.ringneck;

import static java.lang.String.format;
import static java.sql.DriverManager.getConnection;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;

import uk.ac.glasgow.ringneck.builder.BuildResult;

public class SQLiteDatabaseOfReportsWriter {
	
	private static final Logger logger = Logger.getLogger(SQLiteDatabaseOfReportsWriter.class);
	
	private String connectionString; 
	private Connection connection;
	
	public SQLiteDatabaseOfReportsWriter(File databaseFile){
		this.connectionString = "jdbc:sqlite:"+databaseFile.getPath();
	}
	
	public void processExperiments(List<RingneckReport> reports){
		
		logger.info(
			format(
				"Logging reports [%s] to database at connection string [%s].", 
				reports,
				connectionString));
				
		openConnection();
		
		for (RingneckReport report : reports){
			insertExperimentInEstablishedDatabase(report);
		}
		
		closeConnection();
		
		logger.info("Finished logging reports.");
		
	}
	
	public void processExperiment(RingneckReport report){
				
		openConnection();
		
		insertExperimentInEstablishedDatabase(report);
		
		closeConnection();
		
	}
	
	public void insertExperimentInEstablishedDatabase(RingneckReport report){
		try {
			logger.info(
				format(
					"Logging report [%s] to database at connection string [%s].", 
					report,
					connectionString));

			Integer experimentalReportId =
				insertExperimentalReport(connection, report);

			if (report.projectResult.equals(ProjectResult.ANALYSIS_COMPLETED)){
				
				Set<Entry<DependencyMutation, BuildResult>> mutantResults =
					report.mutantResults.entrySet();
				
				for (Entry<DependencyMutation,BuildResult> mutantResult : mutantResults)
					insertMutantResult(connection, experimentalReportId, mutantResult);
			}
			
			logger.info(
				format(
					"Finished logging ringneck report with id [%d].", 
					experimentalReportId));

	
		} catch (SQLException e) {
			logger.error("Couldn't log experiment report.", e);
		}

	}
	
	private void openConnection() {
		
		try {
			connection = getConnection(connectionString);				
			ensureTablesExist(connection);
		} catch (Exception e) {
			logger.error(format("While creating connection [%s]", connectionString), e);
			//Should really throw an API Level exception here instead.
		} 
	}
	
	private void closeConnection() {
		if (connection != null)
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(
					format(
						"While closing connection [%s]", connectionString), e);
			}
	}
	
	private Integer insertExperimentalReport(
		Connection connection, RingneckReport report) throws SQLException {
				
		String statementTemplate = 
			"INSERT INTO experimental_report ("
				+ "scm_connection, "
				+ "group_id, "
				+ "artifact_id, "
				+ "version, "
				+ "project_result"
			+ ") VALUES(?,?,?,?,?) ";
		
		PreparedStatement preparedStatement =
			connection.prepareStatement(statementTemplate);
			
		preparedStatement.setString(1, report.scmConnectionString);
		preparedStatement.setString(2, report.groupId);
		preparedStatement.setString(3, report.artifactId);
		preparedStatement.setString(4, report.version);
		preparedStatement.setString(5, report.projectResult.toString());

		preparedStatement.execute();
		preparedStatement.close();
		
		Statement statement = connection.createStatement();
		String query = "SELECT MAX(rowid) FROM experimental_report";
		ResultSet resultSet = 
			statement.executeQuery(query);
		resultSet.next();
		
		Integer result = resultSet.getInt(1);
		statement.close();
		return result;
	}


	private void insertMutantResult(
		Connection connection, 
		Integer experimentalReportId, 
		Entry<DependencyMutation, BuildResult> mutantResult) {

		String statementTemplate = 
			"INSERT INTO mutant_result ("
				+ "experimental_report, "
				+ "group_id, "
				+ "artifact_id, "
				+ "version_range, "
				+ "mutant_version, "
				+ "mutant_result"
			+ ") VALUES (?,?,?,?,?,?)";

		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(statementTemplate);

			Dependency original = mutantResult.getKey().original;
			statement.setInt(1, experimentalReportId);
			statement.setString(2, original.getGroupId());
			statement.setString(3, original.getArtifactId());
			
			Model referenceModel = mutantResult.getKey().referenceModel;
			
			
			String originalVersion = new ModelHelper(referenceModel).getDependencyVersion(original);
			
			statement.setString(4, originalVersion);
			statement.setString(5, mutantResult.getKey().mutant.getVersion());
			statement.setString(6, mutantResult.getValue().toString());
			
			statement.execute();
		
		} catch (SQLException e) {
			logger.error(format("While executing statement [%s].",statement), e);
		} finally {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error(format("While closing statement [%s].",statement), e);
			}
		}
	}


	private void ensureTablesExist(Connection connection) {
		
		Statement statement = null;
	
		try {
			statement = connection.createStatement();
		
			String experimentalReportSchema =
				"experimental_report ("
					+ "scm_connection TEXT,"
					+ "group_id TEXT, "
					+ "artifact_id TEXT, "
					+ "version TEXT, "
					+ "project_result TEXT"
					+")";
		
			statement.execute("CREATE TABLE IF NOT EXISTS "+experimentalReportSchema);
		
			String mutantResultSchema =
				"mutant_result ("
					+ "experimental_report INTEGER REFERENCES experimental_report (rowid), "
					+ "group_id TEXT, "
					+ "artifact_id TEXT, "
					+ "version_range TEXT, "
					+ "mutant_version TEXT, "
					+ "mutant_result TEXT "
					+ ")";
		
		
			statement.execute("CREATE TABLE IF NOT EXISTS "+mutantResultSchema);
		
		} catch (SQLException e) {
			logger.error(format("While creating report tables."), e);
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					logger.error(format("While closing statement [%s].",statement), e);
				}	
			}
	}

	
}