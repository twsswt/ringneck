package uk.ac.glasgow.ringneck;

import static java.lang.String.format;
import static uk.ac.glasgow.ringneck.ModelHelper.readModelFromFile;
import static uk.ac.glasgow.ringneck.ProjectResult.ANALYSIS_COMPLETED;
import static uk.ac.glasgow.ringneck.ProjectResult.DEFAULT_CONFIGURATION_FAILED;
import static uk.ac.glasgow.ringneck.ProjectResult.NO_DEPENDENCIES;
import static uk.ac.glasgow.ringneck.ProjectResult.NO_MODEL;
import static uk.ac.glasgow.ringneck.ProjectResult.UNDEPLOYABLE;
import static uk.ac.glasgow.ringneck.builder.BuildResult.NOT_ATTEMPTED;
import static uk.ac.glasgow.ringneck.builder.BuildResult.SUCCESS;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapper;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapperFactory;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerException;

/**
 * Manages the execution of the ringneck workflow.  The process is as follows.
 * 
 *  1. Deploy a copy of a project into a working directory.  This can 
 *     be from an Scm such as Git or subversion, or from a release archive.
 *  2. Check for the presence of a Project Object Model (POM) file. If not 
 *     found then exit.
 *  3. Execute the clean test maven goals on the project reference POM. If 
 *     this fails the exit.
 *  4. Generate a population of mutant POMs for each mono-variant component 
 *     assembly.
 *  5. For each mutant, reset the working directory, write the variant POM 
 *     to the working directory and execute the clean test goal again.
 *  
 *  Results for all variants are recorded and accessible during the execution 
 *  of the run.
 * 
 * @author tws
 *
 */
public class RingneckMethod {
	
	private static final Logger logger = Logger.getLogger(RingneckMethod.class);
		
	private final File workingCopyDirectory;
	private final File projectDirectory;
	
	private final File pomFile;	
	public final ScmDeployer scmDeployer;
		
	private final Long defaultBuildTimeoutMilliseconds;
	private final Integer mutantTimeoutFactor;
	
	
	private Map<DependencyMutation,BuildResult> mutantResults;

	private MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessWrapperFactory;
	
	/** 
	 * @param workingCopyDirectory
	 * @param scmDeployer
	 * @param projectDirectory the directory within the workspace
	 * where the project is located. This is useful if the
	 * project contains a number of sub-projects that cannot
	 * be deployed independently.
	 * @param mavenBuildRunnerProcessWrapperFactory
	 * @param defaultBuildTimeoutMilliseconds
	 * @param mutantTimeoutFactor
	 */
	public RingneckMethod (
		File workingCopyDirectory,
		ScmDeployer scmDeployer,
		File projectDirectory,
		MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessWrapperFactory,
		Long defaultBuildTimeoutMilliseconds,
		Integer mutantTimeoutFactor){
		
		this.workingCopyDirectory = workingCopyDirectory;
		this.pomFile = new File (projectDirectory.getPath()+"/pom.xml");
		this.scmDeployer = scmDeployer;
		
		this.projectDirectory = projectDirectory;
				
		this.mavenBuildRunnerProcessWrapperFactory = 
			mavenBuildRunnerProcessWrapperFactory;

		this.defaultBuildTimeoutMilliseconds = defaultBuildTimeoutMilliseconds;
		this.mutantTimeoutFactor = mutantTimeoutFactor;
		
	}
	
	public ProjectResult run () throws RingneckException {
		
		logger.info(
			format(
				"Attempting deployment of project to directory [%s].", workingCopyDirectory));
		
		try {
			scmDeployer.deployTo(workingCopyDirectory);
			logger.info(format("Finished deploying project to directory [%s].", workingCopyDirectory));
		} catch (ScmDeployerException e){

			String messageTemplate = 
				"Couldn't deploy project to directory [%s] using Scm deployer [%s].";
			String message = 
				format(messageTemplate, workingCopyDirectory, scmDeployer.toString());
			
			logger.warn(message, e);
			return UNDEPLOYABLE;
		}

		logger.info(format("Deployed project to [%s].", workingCopyDirectory));
		
		if (!pomFile.exists()){
			logger.info(
				format(
					"Project in directory [%s] has no pom.xml in deployed directory, ending analysis.",
					projectDirectory));
			return NO_MODEL;			
		}

		ModelHelper deployedModelHelper;

		try {
			deployedModelHelper = readModelFromFile(pomFile);
		} catch (IOException | XmlPullParserException e) {
			logger.warn(format("Couldn't read pom file from project directory [%s]", projectDirectory));
			return NO_MODEL;
		}
		
		Model deployedModel = deployedModelHelper.getModel();
			
		String projectName = deployedModelHelper.getProjectName();
		
		if (deployedModel.getDependencies().size() < 1){
			logger.info(format("Deployed project model [%s] has no dependencies, skipping.",projectName));
			return NO_DEPENDENCIES;
		}
		
		logger.info(
			format(
				"Running test goal on project [%s] default dependency configuration in directory [%s].",
				projectName,projectDirectory));
				
		MavenBuildRunnerProcessWrapper wrapper = 
			 mavenBuildRunnerProcessWrapperFactory.
				createBuildProcessWrapper(defaultBuildTimeoutMilliseconds);
		
		BuildResult buildResult = wrapper.buildProject();
		Long referenceDuration = wrapper.getDuration();
		
		if (!buildResult.equals(SUCCESS)) {
			
			logger.info(
				format(
					"Default configuration of [%s] failed with result [%s], skipping mutation testing.",
					projectName, buildResult));
			
			return DEFAULT_CONFIGURATION_FAILED;
		} else {
			logger.info(
				format(
					"Default configuration built successfully in [%d] milliseconds.",
					referenceDuration));
		}
		
		long mutantDuration = referenceDuration * mutantTimeoutFactor;
		
		logger.info(
			format(
				"Starting analysis of mutant configuration for project [%s] allowing [%d] milliseconds for build and test.",
				projectName, mutantDuration));
		
		return analyseMutants(deployedModelHelper, mutantDuration);

	}

	private ProjectResult analyseMutants(ModelHelper modelHelper, Long timeout) throws RingneckException {
		
		String projectName =
			modelHelper.getProjectName();
				
		Set<DependencyMutation> mutantPopulation = 
			modelHelper.getMutantPopulation();
				
		logger.info(format("Found [%d] mutants for project [%s].", mutantPopulation.size(), projectName));
		
		mutantResults = new HashMap<DependencyMutation,BuildResult>();
		for (DependencyMutation mutantRelation : mutantPopulation){

			try {
				scmDeployer.deployTo(workingCopyDirectory);
			} catch (ScmDeployerException e) {
				String messageTemplate = 
					"Couldn't reset project in directory [%s].";
				
				String message = 
					format(messageTemplate, workingCopyDirectory);
				
				logger.warn(message, e);
			}
			
			BuildResult buildResult = analyseMutant(mutantRelation.mutantModel, modelHelper, timeout);
			mutantResults.put(mutantRelation, buildResult);
		}
		return ANALYSIS_COMPLETED;
	}

	private BuildResult analyseMutant(Model mutant, ModelHelper modelHelper, Long timeout) throws RingneckException {
		
		Set<Dependency> mutatedDependencies = 
			modelHelper.getDependencyDifferenceSet (mutant);

		logger.info(format("Starting testing mutant configuration [%s].", mutatedDependencies));
		
		try {
			new ModelHelper(mutant).writeModelToPOM(pomFile);
		} catch (IOException e) {
			logger.warn(
				format(
					"Couldn't write POM file [%s] for mutant [%s].",
					pomFile, mutatedDependencies),
				e);
			return NOT_ATTEMPTED;
		}
		
		MavenBuildRunnerProcessWrapper mutantWrapper = 
			mavenBuildRunnerProcessWrapperFactory.createBuildProcessWrapper(timeout);

		try {
			BuildResult buildResult = mutantWrapper.buildProject();
			
			logger.info(
					format("Finished testing mutant configuration [%s]. Result was [%s].",
						mutatedDependencies, buildResult));
			return buildResult;
		} catch (RingneckException e) {
			logger.info(format("Mutant [%s] couldn't be tested.", mutatedDependencies), e);
			return NOT_ATTEMPTED;
		} 
	}
	
	public Map<DependencyMutation,BuildResult> getMutantResults() {
		return mutantResults;
	}

	public Long getTotalPossibleVariants() throws RingneckException {
		try {
			ModelHelper deployedModelHelper = readModelFromFile(pomFile);
			return deployedModelHelper.getTheoreticalMutantPopulationSize();
		} catch (IOException | XmlPullParserException e) {
			throw new RingneckException (
				format("Couldn't read pom file from project directory [%s]", projectDirectory), e);
		}
	}
	
}
