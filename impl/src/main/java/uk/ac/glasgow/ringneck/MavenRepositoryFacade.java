package uk.ac.glasgow.ringneck;

import java.util.List;

import org.apache.maven.model.Dependency;
import org.codehaus.plexus.PlexusContainer;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.DependencyGraphTransformer;
import org.eclipse.aether.collection.DependencyManager;
import org.eclipse.aether.collection.DependencySelector;
import org.eclipse.aether.collection.DependencyTraverser;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.LocalRepositoryManager;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.repository.RemoteRepository.Builder;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.util.graph.manager.ClassicDependencyManager;
import org.eclipse.aether.util.graph.selector.StaticDependencySelector;
import org.eclipse.aether.util.graph.transformer.NoopDependencyGraphTransformer;
import org.eclipse.aether.util.graph.traverser.StaticDependencyTraverser;
import org.eclipse.aether.version.Version;

public class MavenRepositoryFacade {
	
	private RepositorySystem repositorySystem;
	private RepositorySystemSession repositorySystemSession;
	private RemoteRepository remoteRepository;
	
	private static MavenRepositoryFacade singleton;
	
	public static MavenRepositoryFacade getMavenRepositoryFacade() {
		if (singleton == null)
			try {
				singleton = new MavenRepositoryFacade();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return singleton;
	}
	
	private MavenRepositoryFacade () throws Exception {
		repositorySystem = createRepositorySystem();
		
		repositorySystemSession =
			MavenRepositoryFacade.createDefaultRepositorySystemSession(repositorySystem);
		
		remoteRepository = MavenRepositoryFacade.createRemoteRepository();
	}
	
	public ArtifactResult retrievePomArtifact(Artifact artifact)
		throws ArtifactResolutionException {
			
		ArtifactRequest request = new ArtifactRequest();
		request.setArtifact(artifact);
		request.addRepository(remoteRepository);
		
		request.setRequestContext("runtime");
			
		ArtifactResult result = 
			repositorySystem.resolveArtifact(
				repositorySystemSession, request);
			
		return result;
	}
	
	public List<Version> getAvailableDependencyVersions(Dependency dependency)
		throws VersionRangeResolutionException {
		
		String groupId = dependency.getGroupId();
		String artifactId = dependency.getArtifactId();
		
		return getAvailableDependencyVersions(groupId, artifactId);
	}
	
	public List<Version> getAvailableDependencyVersions(String groupId, String artifactId)
		throws VersionRangeResolutionException {
		
		VersionRangeRequest rangeRequest = 	createRangeRequest(groupId, artifactId);
		
		VersionRangeResult rangeResult = 
			repositorySystem.resolveVersionRange(
					repositorySystemSession, rangeRequest );
			
		return rangeResult.getVersions();
	}
	
	private VersionRangeRequest createRangeRequest(String groupId, String artifactId){
		Artifact artifact =
			new DefaultArtifact( groupId, artifactId, "","[0,)" );
		
		VersionRangeRequest rangeRequest = new VersionRangeRequest();
		rangeRequest.addRepository(remoteRepository);

		rangeRequest.setArtifact( artifact );

		return rangeRequest;
	}
	
	private static DefaultRepositorySystemSession 
		createDefaultRepositorySystemSession(RepositorySystem repositorySystem) {
		
		DefaultRepositorySystemSession repositorySystemSession =
			new DefaultRepositorySystemSession();
						
		DependencyTraverser traverser = new StaticDependencyTraverser(false);
		repositorySystemSession.setDependencyTraverser(traverser);
		
		DependencyManager manager = new ClassicDependencyManager();
		repositorySystemSession.setDependencyManager(manager);
		
		DependencySelector selector = new StaticDependencySelector(true);
		repositorySystemSession.setDependencySelector(selector);
		
		DependencyGraphTransformer transformer =
			new NoopDependencyGraphTransformer();
		repositorySystemSession.setDependencyGraphTransformer(transformer);
		
		LocalRepository localRepository =
			new LocalRepository( "target/local-repo" );
		
		LocalRepositoryManager localRepositoryManager = 
			repositorySystem.newLocalRepositoryManager(
				repositorySystemSession, localRepository );
		
		repositorySystemSession.setLocalRepositoryManager(localRepositoryManager);
		
		return repositorySystemSession;
	}
	
	private static RepositorySystem createRepositorySystem() throws Exception {
		
		PlexusContainer container = 
			PlexusFacade.createPlexusContainer();
		
		return container.lookup(RepositorySystem.class);

	}
	
	private static RemoteRepository createRemoteRepository() {
		Builder remoteRepositoryBuilder =
			new RemoteRepository.Builder(
				"central", "default", "http://repo1.maven.org/maven2/");
		
		return
			remoteRepositoryBuilder.build();
	}
}
