package uk.ac.glasgow.ringneck;

import static java.lang.String.format;
import static java.nio.charset.Charset.defaultCharset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.AbstractList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ProjectPopulationFromMavenJSONQuery extends AbstractList<ModelHelper> {
	
	private final JSONArray mavenRepositoryEntries;
	
	private Map<JSONObject,ModelHelper> modelCache;

	private Logger logger = Logger.getLogger(ProjectPopulationFromMavenJSONQuery.class);
	
	public ProjectPopulationFromMavenJSONQuery (File file) throws FileNotFoundException {
		this(new FileInputStream(file));
	}
	
	public ProjectPopulationFromMavenJSONQuery (URL url) throws IOException {
		this(url.openStream());
	}
	
	public ProjectPopulationFromMavenJSONQuery (InputStream inputStream) {
		
		Reader reader = new InputStreamReader(inputStream, defaultCharset());
		
		JSONTokener jSONTokener = new JSONTokener(reader);
		mavenRepositoryEntries =
			new JSONObject(jSONTokener).getJSONObject("response").getJSONArray("docs");
		
		modelCache = new HashMap<JSONObject,ModelHelper>();
	}


	@Override
	public ModelHelper get(int index) {
		JSONObject mavenRepositoryEntry = 
			mavenRepositoryEntries.getJSONObject(index);
		
		ModelHelper modelHelper = modelCache.get(mavenRepositoryEntry);
		
		if (modelHelper == null){	
			try {
				modelHelper = retrieveModel(mavenRepositoryEntry);
			} catch (IOException | XmlPullParserException e) {
				logger .warn(format("Couldn't parse object model for entry [%s]",mavenRepositoryEntry));
				return null;
			}
			modelCache.put(mavenRepositoryEntry, modelHelper);
		}
		return modelHelper;
	}
	
	@Override
	public Iterator<ModelHelper> iterator() {
		return new ProjectPopulationIterator();
	}

	@Override
	public int size() {
		return this.mavenRepositoryEntries.length();
	}
	
	private ModelHelper retrieveModel(JSONObject mavenRepositoryEntry) throws IOException, XmlPullParserException {
		Artifact artifact =
			createPopulationArtifact(mavenRepositoryEntry);
			
		MavenRepositoryFacade mavenRepositoryWrapper =
			MavenRepositoryFacade.getMavenRepositoryFacade();
		
		ArtifactResult artifactResult;
		try {
			artifactResult = mavenRepositoryWrapper.retrievePomArtifact(artifact);
		} catch (ArtifactResolutionException e) {
			return null;
		}
		Artifact pomArtifact = artifactResult.getArtifact();
		File pomFile = pomArtifact.getFile();
		
		return ModelHelper.readModelFromFile(pomFile);

	}
	
	private  Artifact createPopulationArtifact(JSONObject mavenRepositoryEntry) {
		String id = mavenRepositoryEntry.getString("id");
		String[] idSplit = id.split(":");
		String groupId = idSplit[0];
		String artifactId = idSplit[1];
		String version = mavenRepositoryEntry.getString("latestVersion");
							
		Artifact artifact =
			new DefaultArtifact( groupId, artifactId, "","pom", version );

		return artifact;
	}

	public class ProjectPopulationIterator implements Iterator<ModelHelper> {

		Integer index = 0;
		
		@Override
		public boolean hasNext() {
			return index < size();
		}
		
		@Override
		public void remove (){
			throw new UnsupportedOperationException();
		}

		@Override
		public ModelHelper next() {
			if (hasNext()) 
				return get (index++);
			else throw new NoSuchElementException();
		}
				
	}
}
