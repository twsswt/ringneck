package uk.ac.glasgow.ringneck.builder;

public enum BuildResult {
	SUCCESS, FAILED_TEST, FAILED_COMPILE, TIMED_OUT, NOT_ATTEMPTED, UNKNOWN}
