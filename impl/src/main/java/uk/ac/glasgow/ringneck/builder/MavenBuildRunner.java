package uk.ac.glasgow.ringneck.builder;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static uk.ac.glasgow.ringneck.builder.BuildResult.FAILED_COMPILE;
import static uk.ac.glasgow.ringneck.builder.BuildResult.FAILED_TEST;
import static uk.ac.glasgow.ringneck.builder.BuildResult.SUCCESS;
import static uk.ac.glasgow.ringneck.builder.BuildResult.UNKNOWN;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.maven.Maven;
import org.apache.maven.cli.logging.Slf4jConfiguration;
import org.apache.maven.cli.logging.Slf4jConfigurationFactory;
import org.apache.maven.execution.AbstractExecutionListener;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.ExecutionEvent;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionResult;
import org.apache.maven.plugin.MojoExecution;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.PlexusContainerException;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

import uk.ac.glasgow.ringneck.PlexusFacade;
import uk.ac.glasgow.ringneck.RingneckException;

/**
 * Maven has to be run in the base directory of a project, so this Java main class
 * is executed by the main Corncrake tool on a per project directory basis.
 * 
 * @author tws
 *
 */

class MojoFailedExecutionEventCatcher extends AbstractExecutionListener {
	private ExecutionEvent executionEvent;

	@Override
	public void mojoFailed(ExecutionEvent arg0) {
		this.executionEvent = arg0;
	}
	
	public ExecutionEvent getExecutionEvent (){
		return executionEvent;
	}

}

public class MavenBuildRunner {
	
	private static final Logger logger = Logger.getLogger(MavenBuildRunner.class);

	private static MojoFailedExecutionEventCatcher eventCatcher;

	public static void main(String[] args) {
				
		configure(args[0]);
		
		logger.info(format("Executing maven in directory [%s].", new File(".").getAbsolutePath()));		
		try {
			MavenExecutionResult result = buildProject();
				
			if (result.hasExceptions()){
				
				ExecutionEvent executionEvent = eventCatcher.getExecutionEvent();
				
				if (executionEvent == null){
					String message ="Exception occured in maven build outside of Mojo Event.";
					for (Throwable exception : result.getExceptions()){
						logger.info(message, exception);
					}
					System.exit(UNKNOWN.ordinal());
				}
				
				MojoExecution mojoExecution = 
					executionEvent.getMojoExecution();

				String goal = mojoExecution.getGoal();
				String lifecyclePhase = mojoExecution.getLifecyclePhase();
				
				logger.info(
					format(
						"Build failed due to failed goal [%s] in lifecycle phase [%s].", 
						goal, lifecyclePhase));
				
				if (goal.equals("compile"))
					System.exit(FAILED_COMPILE.ordinal());
				else if (goal.equals("test"))
					System.exit(FAILED_TEST.ordinal());
				else 
					System.exit(UNKNOWN.ordinal());
			}
		} catch (RingneckException e) {
			System.exit(UNKNOWN.ordinal());
		}
		System.exit(SUCCESS.ordinal());
	}

	public static MavenExecutionResult buildProject() throws RingneckException {

		PlexusContainer plexusContainer;
		try {
			plexusContainer = PlexusFacade.createPlexusContainer();
			
		} catch (PlexusContainerException e) {
			throw new RingneckException(
				"Couldn't initialise Plexus component container.", e);
		}
		
		Maven maven;
		try{ 
			maven = plexusContainer.lookup(Maven.class);
			
		} catch (ComponentLookupException e){
			throw new RingneckException(
				"Couldn't initialise maven assembly in component container.", e);
		}
		
		DefaultMavenExecutionRequest request = createDefaultMavenExecutionRequest();

		MavenExecutionResult result = maven.execute(request);
		if (plexusContainer != null)
			plexusContainer.dispose();
		return result;
	}

	private static DefaultMavenExecutionRequest createDefaultMavenExecutionRequest() {

		DefaultMavenExecutionRequest request = new DefaultMavenExecutionRequest();
		
		ILoggerFactory slf4jLoggerFactory = 
			LoggerFactory.getILoggerFactory();
		Slf4jConfiguration slf4jConfiguration = 
			Slf4jConfigurationFactory.getConfiguration( slf4jLoggerFactory );
		
		request.setLoggingLevel(MavenExecutionRequest.LOGGING_LEVEL_DEBUG);
		slf4jConfiguration.activate();		

		request.setReactorFailureBehavior(MavenExecutionRequest.REACTOR_FAIL_FAST);
		request.setGlobalChecksumPolicy(MavenExecutionRequest.CHECKSUM_POLICY_WARN);
				
		request.setInteractiveMode(false);
		request.setRecursive(true);
		request.setShowErrors(false);
		request.setOffline(false);
		request.setUpdateSnapshots(false);
		request.setNoSnapshotUpdates(false);
		request.setCacheNotFound(true);
		request.setCacheTransferError(false);
		
		System.setProperty("rat.skip", "true");
		
		request.setSystemProperties(System.getProperties());
		List<String> goals = asList(new String[] { "clean", "test" });
		request.setGoals(goals);

		request.setPom(new File("pom.xml"));
	
		eventCatcher = new MojoFailedExecutionEventCatcher();
		
		request.setExecutionListener(eventCatcher);
		
		
		return request;
	}

}