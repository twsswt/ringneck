package uk.ac.glasgow.ringneck.scm;

import java.io.File;

public interface ScmDeployer {
		
	public abstract void deployTo (File workingCopyPath) throws ScmDeployerException;

	public abstract String getScmConnectionString();

	public abstract String getScmTag();
}
