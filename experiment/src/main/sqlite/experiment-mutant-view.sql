CREATE VIEW IF NOT EXISTS experiment_mutant_view AS
SELECT 
  experimental_report.group_id AS experimental_report_group_id, 
  experimental_report.artifact_id AS experimental_report_artifact_id, 
  experimental_report.version, 
  mutant_result.group_id, 
  mutant_result.artifact_id, 
  mutant_result.version_range, 
  mutant_result.mutant_version, 
  mutant_result 
FROM experimental_report 
LEFT JOIN mutant_result ON experimental_report.rowid = mutant_result.experimental_report 
WHERE project_result = 'ANALYSIS_COMPLETED' AND NOT mutant_result IS NULL;
