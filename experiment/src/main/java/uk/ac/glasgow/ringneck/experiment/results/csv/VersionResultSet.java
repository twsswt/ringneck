package uk.ac.glasgow.ringneck.experiment.results.csv;

import static java.lang.Math.max;
import static java.lang.String.format;
import static java.util.Collections.sort;
import static org.apache.maven.artifact.versioning.VersionRange.createFromVersionSpec;
import static uk.ac.glasgow.ringneck.builder.BuildResult.SUCCESS;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;

import uk.ac.glasgow.ringneck.builder.BuildResult;

public class VersionResultSet {
	
	private class VersionComparator	implements Comparator<String> {
		
		@Override
		public int compare(String o1, String o2) {
			ComparableVersion cv1 = new ComparableVersion(o1);
			ComparableVersion cv2 = new ComparableVersion(o2);

			return cv1.compareTo(cv2);
		}

	}
	
	private final Map<String,BuildResult> dependencyVersionResultMap;
	
	private final List<String> sortedVersions;
	
	
	public VersionResultSet (Map<String,BuildResult> dependencyVersionResultMap){
		this.dependencyVersionResultMap = new HashMap<String,BuildResult>();
		this.dependencyVersionResultMap.putAll(dependencyVersionResultMap);
		this.sortedVersions = getSortedVersions(this.dependencyVersionResultMap);
	}
	
	private List<String> getSortedVersions(
		Map<String, BuildResult> dependencyVersionResultMap) {
		List<String> sortedVersions = new ArrayList<String>(dependencyVersionResultMap.keySet());
		sort(sortedVersions, new VersionComparator());
		return sortedVersions;
	}
	
	public VersionRange getKnownVersionRange() 
			throws InvalidVersionSpecificationException {
		
		String lowerBoundVersion = sortedVersions.get(0);
		String upperBoundVersion = sortedVersions.get(max(0, sortedVersions.size()-1));
		
		if (lowerBoundVersion.equals(upperBoundVersion)){
			String versionRangeSpecification =
				format("[%s]",lowerBoundVersion);
			return createFromVersionSpec(versionRangeSpecification);
			
		}else {
		
			String versionRangeSpecification =
				format("[%s,%s]",lowerBoundVersion, upperBoundVersion);
			return createFromVersionSpec(versionRangeSpecification);
		}
	}

	public VersionRange getOptimalVersionRange() 
			throws InvalidVersionSpecificationException {
		
		StringBuffer versionRangeSpecification = new StringBuffer();
				
		Integer lowerBoundIndex = 0;
		while (lowerBoundIndex < sortedVersions.size()){			
			
			String lowerBoundVersion = sortedVersions.get(lowerBoundIndex);
			BuildResult rangeType = dependencyVersionResultMap.get(lowerBoundVersion);
			
			Integer upperBoundIndex = lowerBoundIndex;
			while (
				upperBoundIndex < sortedVersions.size() &&
				matchRangeTypeCategory(
					rangeType,
					dependencyVersionResultMap.get(sortedVersions.get(upperBoundIndex)))){
				
				upperBoundIndex ++;
				
			}
			String upperBoundVersion = sortedVersions.get(upperBoundIndex-1);
			
			if (rangeType.equals(SUCCESS))
				extendVersionRangeSpecification(
					versionRangeSpecification, lowerBoundVersion, upperBoundVersion);
			
			lowerBoundIndex = upperBoundIndex;
		}
		
		return createFromVersionSpec(versionRangeSpecification.toString());
	}

	private void extendVersionRangeSpecification(StringBuffer versionRangeSpecification, String lower, String upper) {
		if (versionRangeSpecification.length()!=0)
			versionRangeSpecification.append(',');
		
		versionRangeSpecification.append('[');
		if (lower.equals(upper))
			versionRangeSpecification.append(lower);
		else { 
			versionRangeSpecification
				.append(lower)
				.append(',')
				.append(upper);
		}
		versionRangeSpecification.append(']');
	}

	private boolean matchRangeTypeCategory(
		BuildResult rangeType, BuildResult buildResult) {
		
		if (rangeType.equals(SUCCESS))
			return buildResult.equals(SUCCESS);
		else
			return !buildResult.equals(SUCCESS);
		
	}

}
