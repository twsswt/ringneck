package uk.ac.glasgow.ringneck.experiment;

public class SubExperimentBuilder {

	private String groupId;
	private String artifactId;
	private String version;
	private String scmConnectionString;
	private String scmTag;
	private String projectWorkingCopyDirectoryPath;
	private String projectDirPath;
	private Long defaultTimeout;
	private Integer mutantTimeoutFactor;
	
	public SubExperimentBuilder setGroupId(String groupId) {
		this.groupId = groupId;
		return this;
	}
	
	public SubExperimentBuilder setArtifactId(String artifactId) {
		this.artifactId = artifactId;
		return this;
	}

	public SubExperimentBuilder setVersion(String version) {
		this.version = version;
		return this;
	}

	public SubExperimentBuilder setScmConnectionString(String scmConnectionString) {
		this.scmConnectionString = scmConnectionString;
		return this;
	}

	public SubExperimentBuilder setScmTag(String scmTag) {
		this.scmTag = scmTag;
		return this;
	}
	
	public SubExperimentBuilder setProjectWorkingDirectoryPath(String projectWorkingCopyDirectoryPath) {
		this.projectWorkingCopyDirectoryPath = projectWorkingCopyDirectoryPath;
		return this;
	}

	public SubExperimentBuilder setProjectDirPath(String projectDirPath) {
		this.projectDirPath = projectDirPath;
		return this;
	}
	
	public SubExperimentBuilder setDefaultTimeout(Long defaultTimeout) {
		this.defaultTimeout = defaultTimeout;
		return this;
	}

	public SubExperimentBuilder setMutantTimeoutFactor(Integer mutantTimeoutFactor) {
		this.mutantTimeoutFactor = mutantTimeoutFactor;
		return this;
	}	

	public SubExperiment build() {
		return 
			new SubExperiment (
				groupId,
				artifactId, 
				version,
				scmConnectionString,
				scmTag,
				projectWorkingCopyDirectoryPath,
				projectDirPath,
				defaultTimeout,
				mutantTimeoutFactor);

	}

}
