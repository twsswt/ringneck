package uk.ac.glasgow.ringneck.experiment.results.csv;

import static java.lang.String.format;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.version.Version;

import uk.ac.glasgow.ringneck.MavenRepositoryFacade;
import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.experiment.results.DataTable;
import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;

public class DependencyMutantBuildCounts extends DataTable<String, DependencyDataLabel, Object> {
	
	private static final Logger logger = Logger.getLogger(DependencyMutantBuildCounts.class);	
	
	public DependencyMutantBuildCounts(Connection connection) throws SQLException {
		super("dependency-mutation-build-results");
		String queryString  = 
			"SELECT "
			+ " artifact_id, group_id, mutant_result "
			+ "FROM mutant_result";
		
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(queryString);
				
		while (resultSet.next()){
			
			String artifactId = resultSet.getString("artifact_id");
			String groupId = resultSet.getString("group_id");
			
			DependencyDataLabel dependencySpec = new DependencyDataLabel(groupId, artifactId);
			
			String buildResult = 
				BuildResult.valueOf(resultSet.getString("mutant_result")).toString().toLowerCase().replaceAll("_", "");
			
			if (buildResult.equals("timedout") || buildResult.equals("unknown"))
				buildResult = "timedoutunknown";
			
			Integer currentCount = (Integer)getCellDataOrDefault(buildResult,dependencySpec, 0);
			currentCount ++;
			setCellData(buildResult,dependencySpec, currentCount);
		}
		
		//Make sure all cells zeroed.
		for (DependencyDataLabel rowLabel : getRowLabels ()){
			for (String buildResult : getColumnLabels()){
				Integer currentCount = (Integer)getCellDataOrDefault(buildResult,rowLabel, 0);
				setCellData(buildResult,rowLabel, currentCount);
			}
			setCellData("group", rowLabel, rowLabel.groupId);
			setCellData("artifact", rowLabel, rowLabel.artifactId);			
		}	
		
		//Set total projects, and available dependency versions for each row.
		for (DependencyDataLabel rowLabel : getRowLabels()){			
			
			Integer totalMutants =
				getRow(rowLabel).values().stream().filter(v -> !(v instanceof String)).map(v->(Integer)v).reduce(0, (a,b) -> a + b );
						
			setCellData("totalmutants", rowLabel, totalMutants);
			
			Integer resillience = ((Integer)getCellData("success", rowLabel)) * 100 / totalMutants; 
			setCellData("resillience", rowLabel, resillience);
			
			try {
				Integer numberOfVersions = 
					getNumberOfVersions(rowLabel);			
				setCellData("numversions", rowLabel, numberOfVersions);
			} catch (VersionRangeResolutionException e) {
				logger.error(
					format("Couldn't determine number of available versions of dependency [%s].",
						rowLabel), e);
			}
		}
		
		sortRows(new Comparator<DependencyDataLabel>(){

			@Override
			public int compare(DependencyDataLabel r1, DependencyDataLabel r2) {
				Integer d1 = (Integer)getRow(r1).get("resillience");
				Integer d2 = (Integer)getRow(r2).get("resillience");
				return d2.compareTo(d1);
			}
			
		});
	}
		
	private Integer getNumberOfVersions(DependencyDataLabel rowLabel) throws VersionRangeResolutionException{
		
		MavenRepositoryFacade mavenRepositoryFacade =
			MavenRepositoryFacade.getMavenRepositoryFacade();
		
		List<Version> alternateVersions = 
			mavenRepositoryFacade.getAvailableDependencyVersions(rowLabel.groupId, rowLabel.artifactId);
		Integer numberOfVersions = alternateVersions.size();
			
		return numberOfVersions;
	}	
}
