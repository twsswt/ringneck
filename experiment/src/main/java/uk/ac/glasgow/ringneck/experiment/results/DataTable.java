package uk.ac.glasgow.ringneck.experiment.results;

import static java.lang.String.format;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalBuildReportFilePath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * A general purpose class for representing and manipulating 2D tables of data, similar to a spreadsheet.
 * @author tws
 *
 */
public class DataTable<C,R,D> {
	
	private static final Logger logger = Logger.getLogger(DataTable.class);

	private final String csvTableName;
	
	private final ArrayList<R> rowLabels;
	private final ArrayList<C> columnLabels;
	
	private final Map<R,Map<C,D>> rows;
	private final Map<C,Map<R,D>> columns;	
	
	public DataTable(String csvTableName){
		this.csvTableName = csvTableName;
		this.rows = new HashMap<R,Map<C,D>>();
		this.columns = new HashMap<C,Map<R,D>>();
		this.rowLabels = new ArrayList<R>();
		this.columnLabels = new ArrayList<C>();
	}
	
	public void setColumnLabel(Integer column, C columnLabel){
		while (columnLabels.size() <= column)
			columnLabels.add(null);
		
		columnLabels.set(column, columnLabel);
	}
	
	public D getCellDataOrDefault(C columnLabel, R rowLabel, D value) {
		return getColumn(columnLabel).getOrDefault(rowLabel, value);
	}
	
	public D getCellData(C columnLabel, R rowLabel) {
		return getColumn(columnLabel).get(rowLabel);
	}
	
	public void setRowLabel(Integer row, R rowLabel){
		rowLabels.set(row, rowLabel);
	}
	
	public void setCellData(C columnLabel, R rowLabel, D data){
		Map<C,D> row = getRow(rowLabel);
		Map<R,D> column = getColumn(columnLabel);
		row.put(columnLabel, data);
		column.put(rowLabel, data);
	}
	
	public Map<R, D> getColumn(C columnLabel) {
		Map<R,D> column = columns.get(columnLabel);
		if (column == null){
			column = new HashMap<R,D>();
			columns.put(columnLabel, column);
			if (!columnLabels.contains(columnLabel))
				columnLabels.add(columnLabel);
		}
		return column;
	}

	public Map<C, D> getRow(R rowLabel) {
		Map<C,D> row = rows.get(rowLabel);
		if (row == null){
			row = new HashMap<C,D>();
			rows.put(rowLabel, row);
			if (!rowLabels.contains(rowLabel))
				rowLabels.add(rowLabel);
		}
		return row;
	}
	
	public List<C> getColumnLabels() {
		return new ArrayList<C>(columnLabels);
	}
	
	public List<R> getRowLabels() {
		return new ArrayList<R>(rowLabels);
	}


	public String getCSVFormattedData (){
		StringBuffer buffer = new StringBuffer ();
		
		buffer.append("rowTitle");
		
		for (C columnLabel : columnLabels)
			buffer.append(',').append(columnLabel);
		
		buffer.append('\n');
				
		for (R rowLabel : rowLabels){
			buffer.append(rowLabel);
			for (C columnLabel : getColumnLabels()){
				
				D cellData = getCellData(columnLabel, rowLabel);
				buffer.append(',');
				if (cellData != null)
					buffer.append(cellData);
			}
			buffer.append('\n');
		}
			
		return buffer.toString();
	}
	
	public DataTable<R,C,D> transpose() {
		DataTable<R,C,D> transposed = new DataTable<R, C, D> (csvTableName);
		
		transposed.columnLabels.addAll(rowLabels);
		transposed.rowLabels.addAll(columnLabels);
		transposed.columns.putAll(rows);
		transposed.rows.putAll(columns);
		
		return transposed;
	}

	public void sortRows(Comparator<R> comparator) {
		Collections.sort(rowLabels, comparator);		
	}
	
	public void sortColumns(Comparator<C> comparator){
		Collections.sort(columnLabels, comparator);
	}
	
	public String getLabel() {
		return csvTableName;
	}

	public void outputDataToCSVFile () throws FileNotFoundException{
	
		String reportsDirPath = getExperimentalBuildReportFilePath();
		File file = new File(format("%s/%s.csv", reportsDirPath,csvTableName));
		logger.warn(format("Outputting data to CSV file [%s].", file));
		
		FileOutputStream fis = new FileOutputStream (file);
		PrintStream printStream = new PrintStream(fis);
		printStream.print(getCSVFormattedData());
		printStream.close();
		
	}
}