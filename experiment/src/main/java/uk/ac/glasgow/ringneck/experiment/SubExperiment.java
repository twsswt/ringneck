package uk.ac.glasgow.ringneck.experiment;

import static java.lang.String.format;
import static org.apache.log4j.Logger.getLogger;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalBuildOutputFilePath;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalWorkspaceDirPath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import org.apache.log4j.Logger;

import uk.ac.glasgow.ringneck.DependencyMutation;
import uk.ac.glasgow.ringneck.ProjectResult;
import uk.ac.glasgow.ringneck.RingneckException;
import uk.ac.glasgow.ringneck.RingneckMethod;
import uk.ac.glasgow.ringneck.RingneckMethodBuilder;
import uk.ac.glasgow.ringneck.RingneckReport;
import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.builder.MavenBuildRunnerProcessWrapperFactory;
import uk.ac.glasgow.ringneck.scm.ScmDeployer;
import uk.ac.glasgow.ringneck.scm.ScmDeployerException;
import uk.ac.glasgow.ringneck.scm.ScmDeployerFactory;

public class SubExperiment {
	
	private static final Logger logger = getLogger(SubExperiment.class);
		
	public final String groupId;
	public final String artifactId;
	public final String version;
	public final String scmConnectionString;
	
	public final String scmTag;
	
	public final String workingCopyDirectoryPath;
	public final String projectDirectoryPath;
			
	public final Long defaultBuildTimeout;
	public final Integer mutantTimeoutFactor;
	
	public SubExperiment (
		String groupId,
		String artifactId,
		String version,
		String scmConnectionString,
		String scmTag,
		String workingCopyDirectoryPath,
		String projectDirectoryPath,
		Long defaultBuildTimeout,
		Integer mutantTimeoutFactor){
						
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.version = version;
		this.scmConnectionString = scmConnectionString;
		this.scmTag = scmTag;
		this.projectDirectoryPath = projectDirectoryPath;
		
		this.workingCopyDirectoryPath = workingCopyDirectoryPath;
		
		this.defaultBuildTimeout = defaultBuildTimeout;
		this.mutantTimeoutFactor = mutantTimeoutFactor;
		
	}
	
	public RingneckReport runExperiment ()  {
		
		RingneckReport report;
				
		File workingCopyDir =
			new File (getExperimentalWorkspaceDirPath()+"/"+workingCopyDirectoryPath);
		workingCopyDir.getParentFile().mkdirs();

		File projectDirectory =
			new File (getExperimentalWorkspaceDirPath()+"/"+projectDirectoryPath);
		
		File buildOutputFile = 
			new File (getExperimentalBuildOutputFilePath( groupId, artifactId, version));


		ScmDeployerFactory scmDeployerFactory = 
			new ScmDeployerFactory();
				
		ScmDeployer scmDeployer = null;
		try {
			scmDeployer = scmDeployerFactory.createScmDeployer(scmConnectionString, scmTag);
			
		} catch (ScmDeployerException e) {
			logger.info(
				format(
					"Couldn't create experiment for project in directory [%s].", workingCopyDirectoryPath),e);	
			return
				new RingneckReport(groupId, artifactId, version, scmConnectionString, ProjectResult.UNDEPLOYABLE);
		}

		logger.debug(format("Output from build jobs will be appended to file [%s].", buildOutputFile));
		buildOutputFile.getParentFile().mkdirs();
		buildOutputFile.delete();

		FileOutputStream fileOutputStream;
		try {
			fileOutputStream = new FileOutputStream(buildOutputFile, true);
		} catch (FileNotFoundException e) {
			logger.info(format("Couldn't initialise for build process output [%s]", buildOutputFile),e);
			return 
				new RingneckReport(groupId, artifactId, version, scmConnectionString, ProjectResult.UNKNOWN);
		}
		
		MavenBuildRunnerProcessWrapperFactory mavenBuildRunnerProcessFactory = 
			new MavenBuildRunnerProcessWrapperFactory(projectDirectory, fileOutputStream);
		
						
		RingneckMethodBuilder ringneckMethodBuilder =
			new RingneckMethodBuilder ()
				.setScmDeployer(scmDeployer)
				.setWorkingDirectory(workingCopyDir)
				.setProjectDirectory(projectDirectory)
				.setMavenBuildRunnerProcessWrapperFactory(mavenBuildRunnerProcessFactory)
				.setDefaultBuildTimeout(defaultBuildTimeout)
				.setMutantTimeoutFactor(mutantTimeoutFactor);
			
		RingneckMethod ringneckMethod = ringneckMethodBuilder.build();
			
	
		logger.info(
			format("Starting analysis of project in directory [%s].", projectDirectory));
			
		try {
			ProjectResult result = ringneckMethod.run();
			logger.info(
				format("Finished analysis of project in directory [%s]", projectDirectory));
			
			Map<DependencyMutation, BuildResult> mutantResults = 
				ringneckMethod.getMutantResults();				
				report = new RingneckReport(
				groupId, artifactId, version, scmConnectionString, result, mutantResults);
		
		} catch (RingneckException e) {

			logger.warn(
				format("Couldn't evaluate project in directory [%s].",
					workingCopyDirectoryPath), e);
				
				report = new RingneckReport(
					groupId, artifactId, version, scmConnectionString, ProjectResult.UNKNOWN);
		}
			
		try {
			Long totalPossibleVariants = 
				ringneckMethod.getTotalPossibleVariants();
			
			logger.info(
				format(
					"Found [%d] possible variants for project in directory [%s].",
					totalPossibleVariants, projectDirectory));
		} catch (RingneckException e){
			logger.info(
				format(
					"Couldn't determine possible variants for project in directory [%s].",
					projectDirectory));
		}

		return report;

	}
}
