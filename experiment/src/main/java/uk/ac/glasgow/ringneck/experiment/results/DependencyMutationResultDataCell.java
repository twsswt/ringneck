package uk.ac.glasgow.ringneck.experiment.results;

import static org.apache.maven.artifact.versioning.VersionRange.createFromVersionSpec;
import static uk.ac.glasgow.ringneck.builder.BuildResult.NOT_ATTEMPTED;
import static uk.ac.glasgow.ringneck.builder.BuildResult.SUCCESS;
import static uk.ac.glasgow.ringneck.builder.BuildResult.UNKNOWN;
import static uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell.MutationResult.CORRECT;
import static uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell.MutationResult.OVER;
import static uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell.MutationResult.UNDER;
import static uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell.MutationResult.UNTESTED;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;

import uk.ac.glasgow.ringneck.builder.BuildResult;


public class DependencyMutationResultDataCell implements Comparable<DependencyMutationResultDataCell> {
	
	/**
	 * The possible result categories for the analysis of a project dependency.
	 * @author Tim
	 *
	 */
	public enum DependencyCategory {
		CORRECT, OVER_AND_UNDER, OVER, UNDER, UNTESTED, UNKNOWN; 
	}

	/**
	 * The possible result categories for the analysis of a project dependency mutation.
	 * @author Tim
	 *
	 */
	enum MutationResult {CORRECT, OVER, UNDER, UNTESTED}
	
	private final Map<MutationResult,Long> backing;
	
	private final VersionRange versionRange;
	
	public DependencyMutationResultDataCell (String versionRangeSpec) throws InvalidVersionSpecificationException{
		this.backing = new HashMap<MutationResult,Long>();
		versionRange = 
			createFromVersionSpec(versionRangeSpec);
	}

	public void addMutationResult(
		BuildResult buildResult,
		String mutantVersion) {
				
		MutationResult mutationResult = 
			calculateMutantResult(mutantVersion, buildResult);
		
		Long count = backing.getOrDefault(mutationResult, 0l);
		backing.put(mutationResult, count+1);
		
	}
	
	private MutationResult calculateMutantResult(String mutantVersion, BuildResult buildResult) {
		
		Boolean mutantShouldHavePassed = 
			mutantShouldHavePassed(mutantVersion);

		
		if (buildResult.equals(UNKNOWN) || buildResult.equals(NOT_ATTEMPTED))
			return UNTESTED;
		
		Boolean mutantDidPass = buildResult.equals(SUCCESS);
		
		if ( (mutantShouldHavePassed && mutantDidPass)  || (!mutantShouldHavePassed && !mutantDidPass) )
			return CORRECT;
		else if (mutantShouldHavePassed && !mutantDidPass)
			return UNDER;
		else if (!mutantShouldHavePassed && mutantDidPass)
			return OVER;
		else return null;	
	}
	
	private Boolean mutantShouldHavePassed(String mutantVersion) {
		return versionRange != null && 
			versionRange.containsVersion(new DefaultArtifactVersion(mutantVersion));
	}
	
	
	public DependencyCategory getDependencyCategory() {		
		if (dependencyIsCorrect())
			return DependencyCategory.CORRECT;
		else if (dependencyIsOverAndUnderConstrained()){
			return DependencyCategory.OVER_AND_UNDER;
		} else if (dependencyIsOverConstrained()){
			return DependencyCategory.OVER;
		} else if (dependencyIsUnderConstrained()){
			return DependencyCategory.UNDER;
		} else if (dependencyIsUnTested()){
			return DependencyCategory.UNTESTED;
		} else 
			return DependencyCategory.UNKNOWN;
	}

	private Boolean dependencyIsCorrect(){
		return backing.getOrDefault(MutationResult.CORRECT, 0l) > 0l &&
			backing.size() == 1;
	}
	
	private Boolean dependencyIsOverAndUnderConstrained() {
		return dependencyIsOverConstrained() &&
			dependencyIsUnderConstrained();
	}	
	
	private Boolean dependencyIsOverConstrained(){
		return backing.getOrDefault(MutationResult.OVER, 0l) > 0l;
	}

	private Boolean dependencyIsUnderConstrained(){
		return backing.getOrDefault(MutationResult.UNDER, 0l) > 0l;
	}
	
	private Boolean dependencyIsUnTested(){
		return backing.getOrDefault(MutationResult.UNTESTED, 0l) > 0l;
	}
	
	@Override
	public String toString (){
		return getDependencyCategory().toString();
	}

	@Override
	public int compareTo(DependencyMutationResultDataCell o) {
		return getDependencyCategory().compareTo(o.getDependencyCategory());
	}
	
}