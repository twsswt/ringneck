package uk.ac.glasgow.ringneck.experiment;

import static java.lang.String.format;

public class ExperimentalConstants {
		
	public static final String getExperimentalBuildReportFilePath (){
		return "reports/ringneck";
	}
	
	public static final String getExperimentalBuildOutputFilePath(String groupId, String artifactId, String version){
		return format("reports/ringneck/%s-%s-%s-build-output.log",groupId,artifactId,version);
	}
		
	public static final String getExperimentalReportDBFilePath(){
		return "reports/ringneck/ringneck.db";
	}

	public static String getExperimentalWorkspaceDirPath() {
		return "target/workspaces";
	}

}
