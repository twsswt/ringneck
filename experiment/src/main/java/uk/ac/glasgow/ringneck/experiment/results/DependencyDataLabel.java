package uk.ac.glasgow.ringneck.experiment.results;

import static java.lang.String.format;

public class DependencyDataLabel {
	public final String groupId;
	public final String artifactId;
	
	public DependencyDataLabel(String groupId, String artifactId){
		this.groupId = groupId;
		this.artifactId = artifactId;
	}
	
	@Override 
	public String toString (){
		return format("%s:%s", groupId, artifactId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
			* result
			+ ((artifactId == null) ? 0 : artifactId
				.hashCode());
		result = prime * result
			+ ((groupId == null) ? 0 : groupId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DependencyDataLabel other = (DependencyDataLabel) obj;
		if (artifactId == null) {
			if (other.artifactId != null)
				return false;
		} else if (!artifactId.equals(other.artifactId))
			return false;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		return true;
	}
}