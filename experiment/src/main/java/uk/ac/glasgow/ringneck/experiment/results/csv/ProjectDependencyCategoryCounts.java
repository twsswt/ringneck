package uk.ac.glasgow.ringneck.experiment.results.csv;

import java.util.Map;

import uk.ac.glasgow.ringneck.experiment.results.DataTable;
import uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell;
import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.ProjectDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell.DependencyCategory;

public class ProjectDependencyCategoryCounts extends DataTable<String, String, Object> {

	public ProjectDependencyCategoryCounts(
		ProjectDependencyCategorisation projectDependencyCategorisation) {
			
		super("project-dependency-category-counts");
		
		setupColumnLabels();
		
		for (ProjectDataLabel rowLabel : projectDependencyCategorisation.getRowLabels()){
						
			Map<DependencyDataLabel,DependencyMutationResultDataCell> row = projectDependencyCategorisation.getRow(rowLabel);
									
			setCellData("groupid", rowLabel.toString(),  rowLabel.groupId);
			setCellData("artifactid",rowLabel.toString(), rowLabel.artifactId);
			setCellData("version", rowLabel.toString(), rowLabel.version);
			setCellData("numdependencies", rowLabel.toString(), row.size());

			setCellData("correct", rowLabel.toString(),filterCount(row, DependencyCategory.CORRECT));
			setCellData("overandunder", rowLabel.toString(),filterCount(row, DependencyCategory.OVER_AND_UNDER));
			setCellData("over", rowLabel.toString(),filterCount(row, DependencyCategory.OVER));
			setCellData("under", rowLabel.toString(),filterCount(row, DependencyCategory.UNDER));
			setCellData("untested", rowLabel.toString(),filterCount(row, DependencyCategory.UNTESTED));			
		}
				
		for (String columnLabel : new String [] {"correct", "overandunder", "over", "under", "untested", }){
			
			Long total =
				this.getColumn(columnLabel).values().stream().mapToLong(v -> (Long) v).sum();
			
			setCellData(columnLabel, "Total", total);
		}
		
		Long total =
			this.getColumn("numdependencies").values().stream().mapToLong(v -> (Integer) v).sum();
		
		setCellData("numdependencies", "Total", total);
		
	}

	private Long filterCount(
		Map<DependencyDataLabel, DependencyMutationResultDataCell> row, DependencyCategory category) {
				
		return row.entrySet()
			.stream()
			.filter(entry -> entry.getValue().getDependencyCategory().equals(category))
			.count();
	}

	private void setupColumnLabels() {
		setColumnLabel(0, "groupid");
		setColumnLabel(1, "artifactid");
		setColumnLabel(2, "version");
		setColumnLabel(3, "numdependencies");
		setColumnLabel(4, "correct");
		setColumnLabel(5, "overandunder");
		setColumnLabel(6, "over");
		setColumnLabel(7, "under");
		setColumnLabel(8, "untested");
		
	}

}
