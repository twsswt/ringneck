package uk.ac.glasgow.ringneck.experiment.results.csv;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static uk.ac.glasgow.ringneck.MavenRepositoryFacade.getMavenRepositoryFacade;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.version.Version;

import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.experiment.results.DataTable;
	

public class DependencyMutantAgeCategorisation extends DataTable<String, Integer, Number> {
	
	private static final Logger logger = Logger.getLogger(DependencyMutantAgeCategorisation.class);
	
	public DependencyMutantAgeCategorisation(Connection connection) throws SQLException {
		super("dependency-mutation-age-categorisation");
		String queryString  = 
			"SELECT "
			+ " artifact_id, group_id, mutant_result, mutant_version, version_range "
			+ "FROM mutant_result";
		
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(queryString);
				
		while (resultSet.next()){
			
			String artifactId = resultSet.getString("artifact_id");
			String groupId = resultSet.getString("group_id");
			String mutantVersion = resultSet.getString("mutant_version");
			String mutantResult = resultSet.getString("mutant_result");
			String versionRange = resultSet.getString("version_range");
						
			BuildResult buildResult = 
				BuildResult.valueOf(mutantResult);
						
			try {
				Integer rowLabel = 
					calculateMutantAgeRelativeToReferenceVersionRange(artifactId, groupId, mutantVersion, versionRange);
					
				update(rowLabel, buildResult);
					
			} catch (VersionRangeResolutionException e) {
				String message = 
					format("Couldn't determine version range for dependency [%s:%s].",
						groupId, artifactId);
				
				logger.error(message,e);
			}
			
			sortRows(new Comparator<Integer>(){

				@Override
				public int compare(Integer o1, Integer o2) {
					return o1.compareTo(o2);
				}
				
			});
			
		}
		
		//Calculate proportions.
		for (Integer rowLabel : getRowLabels()){
			
			Integer totalForAge = (Integer)getCellData("total", rowLabel);
			Integer successForAge = (Integer) getCellData("success", rowLabel);
			
			setCellData("Proportion", rowLabel, successForAge * 1.0 /totalForAge);
		}
		
	}
	
	public void update(Integer rowLabel, BuildResult buildResult){
		
		Integer totalForAge = (Integer)getCellDataOrDefault("total", rowLabel,0);
		
		totalForAge ++;
		Integer successForAge = (Integer) getCellDataOrDefault("success", rowLabel, 0);
		successForAge += buildResult.equals(BuildResult.SUCCESS)? 1 : 0;
		
		setCellData("total", rowLabel, totalForAge);
		setCellData("success", rowLabel, successForAge);

	}


	private Integer calculateMutantAgeRelativeToReferenceVersionRange(
		String artifactId,
		String groupId,
		String mutantVersion,
		String versionRange)
			throws VersionRangeResolutionException {
		
		List<Version> versions = 
			getMavenRepositoryFacade().getAvailableDependencyVersions(groupId, artifactId);
		
		List<String> versionStrings = versions.stream().map(v -> v.toString()).collect(toList());
		
		//TODO replace with proper repository query.
		mutantVersion = mutantVersion.substring(1, mutantVersion.length()-1);
		
		Integer versionIndex = versionStrings.indexOf(mutantVersion);
		
		Integer startIndex = versionStrings.indexOf(versionRange);
		
		Integer rowLabel = versionIndex - startIndex;
		return rowLabel;
	}

}
