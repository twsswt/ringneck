package uk.ac.glasgow.ringneck.experiment.results.csv;

import static java.lang.String.format;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;

import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.experiment.results.DataTable;
import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell;
import uk.ac.glasgow.ringneck.experiment.results.ProjectDataLabel;

/**
 * An intermediate table for presenting the results for all project - dependency combinations.
 * @author tws
 *
 */
public class ProjectDependencyCategorisation
	extends DataTable<DependencyDataLabel,ProjectDataLabel,DependencyMutationResultDataCell>{
		
	private static final Logger logger = Logger.getLogger(ProjectDependencyCategorisation.class);

	public ProjectDependencyCategorisation(Connection connection) throws SQLException{
		super("project-dependency-categorisation");
		
		String queryString = 
			"SELECT "
			+ " experimental_report_group_id, "
			+ " experimental_report_artifact_id, "
			+ " version, "
			+ " group_id, "
			+ " artifact_id, "
			+ " version_range, "
			+ " mutant_version, "
			+ " mutant_result "
			+ "FROM experiment_mutant_view";
		
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(queryString);
				
		while (resultSet.next()){
			
			String groupId = resultSet.getString("experimental_report_group_id");
			String artifactId = resultSet.getString("experimental_report_artifact_id");
			String version = resultSet.getString("version");
			
			ProjectDataLabel rowLabel = new ProjectDataLabel(groupId, artifactId, version);

			String dependencyArtifactId = resultSet.getString("artifact_id");
			String dependencyGroupId = resultSet.getString("group_id");
			
			DependencyDataLabel columnLabel = 
				new DependencyDataLabel(dependencyGroupId, dependencyArtifactId);
			
			BuildResult buildResult = 
				BuildResult.valueOf(resultSet.getString("mutant_result"));
			
			String versionRange = resultSet.getString("version_range");
			String mutantVersion = resultSet.getString("mutant_version");
			
			DependencyMutationResultDataCell dependencyMutationResultCount = 
				getCellData(columnLabel, rowLabel);
			
			if (dependencyMutationResultCount == null)
				try {
					dependencyMutationResultCount = new DependencyMutationResultDataCell(versionRange);
				} catch (InvalidVersionSpecificationException e) {
					logger.error(
						format("While processing a mutant of dependency [%s].",versionRange));
				}
				
			dependencyMutationResultCount
				.addMutationResult(buildResult,mutantVersion);	
			setCellData(columnLabel, rowLabel, dependencyMutationResultCount);
		}
	}
	
}
