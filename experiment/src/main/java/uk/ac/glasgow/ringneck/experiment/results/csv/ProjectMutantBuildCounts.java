package uk.ac.glasgow.ringneck.experiment.results.csv;

import static java.lang.String.format;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;

import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.experiment.results.DataTable;
import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;

public class ProjectMutantBuildCounts extends DataTable<String,String,Object> {

	public ProjectMutantBuildCounts(Connection connection) throws SQLException {
		super( "project-mutant-build-counts");
		
		this.setupColumnLabels();
				
		String queryString = 
			"SELECT "
			+ "  experimental_report.group_id, "
			+ "  experimental_report.artifact_id, "
			+ "  experimental_report.version, "
			+ "  mutant_result, "
			+ "  version_range, "
			+ "  mutant_version "
			+ "FROM experimental_report "
			+ " LEFT JOIN mutant_result ON experimental_report.rowid = mutant_result.experimental_report "
			+ "WHERE project_result = 'ANALYSIS_COMPLETED' AND NOT mutant_result IS NULL";
		
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(queryString);
		
		while (resultSet.next()){
			
			String artifactId = resultSet.getString("artifact_id");
			String groupId = resultSet.getString("group_id");
			String version = resultSet.getString("version");
			
			String rowLabel = 
				String.format("%s:%s:%s", groupId, artifactId, version);
			
			setCellData("group", rowLabel, groupId);
			setCellData("artifact", rowLabel, artifactId);
			setCellData("version", rowLabel, version);
			
			
			BuildResult buildResult = BuildResult.valueOf(resultSet.getString("mutant_result"));
						
			Boolean mutantShouldHavePassed = 
				mutantShouldHavePassed(resultSet.getString("version_range"), resultSet.getString("mutant_version"));
			
			String columnLabel = 
				createColumnLabel(buildResult,	mutantShouldHavePassed);

			Long count = (Long)this.getCellDataOrDefault(columnLabel, rowLabel, 0l);
			this.setCellData(columnLabel, rowLabel, count+1);
		}
				
		for (String rowLabel : getRowLabels()){
			Long mutantsGenerated = sumData(getRow(rowLabel));
			
			setCellData( "mutantsGenerated", rowLabel, mutantsGenerated);
		}
		
		//Make sure all cells zeroed if not set.
		for (String rowLabel : getRowLabels ()){
			for (String columnLabels : getColumnLabels()){
				Object currentCount = getCellDataOrDefault(columnLabels,rowLabel, 0);
				setCellData(columnLabels,rowLabel, currentCount);
			}
		}	

		
		Long totalMutantsGenerated
			= sumData(getColumn("mutantsGenerated"));
			
		for (String columnLabel : getColumnLabels()){
			Long totalForColumn = 
			sumData(getColumn(columnLabel));
			this.setCellData(columnLabel, "Total", totalForColumn);
			this.setCellData(columnLabel, "ProportionOfGenerated", format("%.0f", 100.0 * totalForColumn / totalMutantsGenerated));

		}

	}

	private Long sumData(Map<String, Object> dataItems) {
		return dataItems.values()
			.stream()
			.filter(v -> v instanceof Long)
			.mapToLong(v -> (Long)v)
			.reduce(0l, (a,b) -> a + b);
	}

	private String createColumnLabel(
		BuildResult buildResult, Boolean mutantShouldHavePassed) {
		
		return (mutantShouldHavePassed? "shouldHavePassed" : "shouldHaveFailed" ) + 
			buildResult.toString().toLowerCase().replaceAll("_", "");
	}

	private Boolean mutantShouldHavePassed(String originalVersion, String mutantVersion) {
		VersionRange versionRange = null;
		try {
			versionRange = VersionRange.createFromVersionSpec(originalVersion);
		} catch (InvalidVersionSpecificationException e) {}
		return versionRange != null && 
			versionRange.containsVersion(new DefaultArtifactVersion(mutantVersion));
	}

	private void setupColumnLabels() {
		this.setColumnLabel(0, "group");
		this.setColumnLabel(1, "artifact");
		this.setColumnLabel(2, "version");
		this.setColumnLabel(3, "mutantsGenerated");
		
		Integer columnIndex = 4;
				
		for (BuildResult buildResult : BuildResult.values())
			setColumnLabel(columnIndex++, createColumnLabel(buildResult, true));
		for (BuildResult buildResult : BuildResult.values())
			setColumnLabel(columnIndex++, createColumnLabel(buildResult, false));		
	}
	
}
