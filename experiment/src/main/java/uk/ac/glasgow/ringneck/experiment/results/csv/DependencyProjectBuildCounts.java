package uk.ac.glasgow.ringneck.experiment.results.csv;

import static java.lang.String.format;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.version.Version;

import uk.ac.glasgow.ringneck.MavenRepositoryFacade;
import uk.ac.glasgow.ringneck.experiment.results.DataTable;
import uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell;
import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.ProjectDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.DependencyMutationResultDataCell.DependencyCategory;

public class DependencyProjectBuildCounts extends DataTable<String, String, Object> {
	
	private static final Logger logger = Logger.getLogger(DependencyProjectBuildCounts.class);	
	
	public DependencyProjectBuildCounts  (
		ProjectDependencyCategorisation projectDependencyCategorisation) 
			throws SQLException {
		
		super("dependency-project-build-counts");
		
		DataTable<ProjectDataLabel,DependencyDataLabel,DependencyMutationResultDataCell> transposed = 
			projectDependencyCategorisation.transpose();
		
		setupColumnLabels();
		
		for (DependencyDataLabel rowLabel : transposed.getRowLabels()){
			setCellData("group", rowLabel.toString(), rowLabel.groupId);
			setCellData("artifact", rowLabel.toString(), rowLabel.artifactId);			
			
			try {
				Integer numberOfVersions = 
					getNumberOfVersions(rowLabel);			
				setCellData("numversions", rowLabel.toString(), numberOfVersions);
			} catch (VersionRangeResolutionException e) {
				logger.error(
					format("Couldn't determine number of available versions of dependency [%s].",
						rowLabel), e);
			}
			
			Map<ProjectDataLabel,DependencyMutationResultDataCell> row = 
				transposed.getRow(rowLabel);
			
			setCellData("numprojects", rowLabel.toString(), row.size());
			
			setCellData("correct", rowLabel.toString(),filterCount(row, DependencyCategory.CORRECT));
			setCellData("overandunder", rowLabel.toString(),filterCount(row, DependencyCategory.OVER_AND_UNDER));
			setCellData("over", rowLabel.toString(),filterCount(row, DependencyCategory.OVER));
			setCellData("under", rowLabel.toString(),filterCount(row, DependencyCategory.UNDER));
			setCellData("untested", rowLabel.toString(),filterCount(row, DependencyCategory.UNTESTED));
		}
				
	}

	private Object filterCount(
		Map<ProjectDataLabel, DependencyMutationResultDataCell> row, DependencyCategory category) {
		
		return 
			row
				.values()
				.stream()
				.filter(v -> v.getDependencyCategory().equals(category))
				.count();
	}

	private void setupColumnLabels() {
		setColumnLabel(0, "group");
		setColumnLabel(1, "artifact");
		setColumnLabel(2, "numversions");
		setColumnLabel(3, "numprojects");
		setColumnLabel(4, "correct");
		setColumnLabel(5, "overandunder");
		setColumnLabel(6, "over");
		setColumnLabel(7, "under");
		setColumnLabel(8, "untested");
	}

	private Integer getNumberOfVersions(DependencyDataLabel rowLabel) throws VersionRangeResolutionException{
					
		MavenRepositoryFacade mavenRepositoryFacade =
			MavenRepositoryFacade.getMavenRepositoryFacade();
		
		List<Version> alternateVersions = 
			mavenRepositoryFacade.getAvailableDependencyVersions(rowLabel.groupId, rowLabel.artifactId);
		Integer numberOfVersions = alternateVersions.size();
			
		return numberOfVersions;
	}			

}
