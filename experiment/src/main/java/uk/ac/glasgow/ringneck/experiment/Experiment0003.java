package uk.ac.glasgow.ringneck.experiment;

import static java.util.Arrays.asList;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalReportDBFilePath;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalBuildReportFilePath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.PropertyConfigurator;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import uk.ac.glasgow.ringneck.RingneckReport;
import uk.ac.glasgow.ringneck.SQLiteDatabaseOfReportsWriter;


@RunWith(Parameterized.class)
public class Experiment0003 {
		
	private SubExperiment subExperiment;
	private RingneckReport report;
	
	@BeforeClass
	public static void setup (){
		PropertyConfigurator.configure("src/main/resource/log4j.properties");		
		File reportDir = new File(getExperimentalBuildReportFilePath());
		reportDir.mkdirs();
	}
	
	/* Experimental parameters */
	
	@Parameterized.Parameters(name="[{0}:{1}:{2} from {3}]")
	public static List<? extends Object[]> data () throws Exception {
		
		String [][] data = {
			//{"junit", "junit", "4.12", "https://github.com/junit-team/junit/", "r4.12", "junit/junit"},
			//{"jenkinsci","jenkins","1.619","https://github.com/jenkinsci/jenkins","jenkins-1.619", "jenkinsci/jenkins"},
			//{"org.easymock", "easymock-parent","3.3.1", "https://github.com/easymock/easymock/", "easymock-3.3.1", "org.easymock/easymock-parent"},
			//{"org.pitest", "pitest-parent","1.1.5", "https://github.com/hcoles/pitest/","pitest-parent-1.1.5","org.pitest/pitest-parent"}
			//{"org.jsoup", "jsoup", "1.8.2", "https://github.com/jhy/jsoup/","jsoup-1.8.2", "org.jsoup/jsoup"}// should work.
			//{"org.findbugs","findbugs","3.0.1", "https://github.com/findbugsproject/findbugs", "3.0.1","org.findbugs/findbugs"}
			//{"org.jfree", "jfreechart2", "4.0.0", "https://github.com/jfree/jfreechart-fse", "master","org.free/jfreechart"}
			//{"org.apache","log4j","1.2","https://github.com/apache/log4j/","v1_2final","org.apache/log4j"}
			
			
 		};
		
		return asList(data);		
	}
		
	public Experiment0003 (
		String groupId,
		String artifactId,
		String version,
		String scmConnectionString,
		String scmTag,
		String projectWorkingDirectoryPath,
		String projectDirPath
		) throws IOException, XmlPullParserException {
		
		subExperiment = 
			new SubExperiment (
				groupId,
				artifactId, 
				version,
				scmConnectionString,
				scmTag,
				projectWorkingDirectoryPath,
				projectDirPath,
				1000l * 60l * 10l,
				10);
	}
						
	@Test
	public void runExperiment ()  {		
		report = subExperiment.runExperiment();
	}
	
	@After
	public void tearDown (){

		SQLiteDatabaseOfReportsWriter databaseOfReportsWriter = 
			new SQLiteDatabaseOfReportsWriter(
				new File(getExperimentalReportDBFilePath()));
		
		databaseOfReportsWriter.processExperiment(report);
	}

}
