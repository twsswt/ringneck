package uk.ac.glasgow.ringneck.experiment.results.csv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;

import uk.ac.glasgow.ringneck.builder.BuildResult;
import uk.ac.glasgow.ringneck.experiment.results.DataTable;

public class OptimalMonoVariants extends DataTable<String, String, String> {

	public OptimalMonoVariants(Connection connection) throws SQLException {
		super( "optimal-mono-variants-maven-aether-provider");
		
		String queryStringForRowLabels = 
			  "SELECT DISTINCT "
			+ " experimental_report_group_id, "
			+ " experimental_report_artifact_id, "
			+ " group_id, "
			+ " artifact_id, "
			+ " version_range "
			+ "FROM experiment_mutant_view "
			+ "WHERE experimental_report_artifact_id = 'maven-aether-provider'";
		
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(queryStringForRowLabels);
		
		while (resultSet.next()){
			
			String projectGroupId = resultSet.getString("experimental_report_group_id");
			String projectArtifactId = resultSet.getString("experimental_report_artifact_id");
			
			String dependencyGroupId = resultSet.getString("group_id");
			String dependencyArtifactId = resultSet.getString("artifact_id");
			
			String referenceVersionRange = resultSet.getString("version_range");
						
			try {
				Map<String,BuildResult>  testedProjectDependencyVersionsAndBuildResults = 
					getKnownProjectDependencyVersionsAndBuildResults(
					connection,
					projectGroupId,
					projectArtifactId,
					dependencyGroupId,
					dependencyArtifactId);
				
				// Can add on the assumption that versionRange is a recommended version. 
				testedProjectDependencyVersionsAndBuildResults.put(referenceVersionRange, BuildResult.SUCCESS);
				
				VersionResultSet versionResultSet =
					new VersionResultSet(testedProjectDependencyVersionsAndBuildResults);
								
				VersionRange knownVersionRange = 
					versionResultSet.getKnownVersionRange();
				
				VersionRange optimalVersionRange = 
					versionResultSet.getOptimalVersionRange();
				
				String rowLabel = projectArtifactId + dependencyGroupId + dependencyArtifactId;
				
				
				if (!knownVersionRange.equals(optimalVersionRange)){

					setCellData("groupid",rowLabel,dependencyGroupId);
					setCellData("artifactid",rowLabel,dependencyArtifactId);
					setCellData("version",rowLabel,referenceVersionRange);
					setCellData("knownVersionRange",rowLabel,knownVersionRange.toString());
					setCellData("optimalVersionRange",rowLabel,optimalVersionRange.toString());
					
				}
								
			} catch (InvalidVersionSpecificationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}

	private Map<String,BuildResult> getKnownProjectDependencyVersionsAndBuildResults(
		Connection connection,
		String projectGroupId,
		String projectArtifactId,
		String dependencyGroupId,
		String dependencyArtifactId
		) throws SQLException, InvalidVersionSpecificationException {
		
		String dependencyResultQueryTemplate =
			"SELECT"
			+ " mutant_version ,"
			+ " mutant_result "
			+ "FROM experiment_mutant_view "
			+ "WHERE experimental_report_group_id = ? "
			+ " AND experimental_report_artifact_id = ? "
			+ " AND group_id = ? "
			+ " AND artifact_id = ?";
		
		PreparedStatement dependencyResultQueryStatement = 
			connection.prepareStatement(dependencyResultQueryTemplate);
		
		dependencyResultQueryStatement.setString(1, projectGroupId);
		dependencyResultQueryStatement.setString(2, projectArtifactId);
		dependencyResultQueryStatement.setString(3, dependencyGroupId);
		dependencyResultQueryStatement.setString(4, dependencyArtifactId);
		
		ResultSet resultSet = dependencyResultQueryStatement.executeQuery();
				
		Map<String,BuildResult> dependencyVersionResultMap =
			new HashMap<String,BuildResult>();
		
		while (resultSet.next()){
			BuildResult buildResult = BuildResult.valueOf(resultSet.getString("mutant_result"));
			String version = resultSet.getString("mutant_version");
			version = version.subSequence(1, version.length()-1).toString();
			dependencyVersionResultMap.put(version, buildResult);	
		}
		
		return dependencyVersionResultMap;
		
	}
	
}
