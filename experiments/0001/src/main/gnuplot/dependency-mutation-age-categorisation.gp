#!/usr/bin/gnuplot

#set terminal png size 1200,800
set terminal epslatex size 12cm,7.5cm

#set yrange [0:500]

set datafile separator ","

set output 'reports/ringneck/dependency-mutation-age-categorisation.tex'

set xlabel 'version mutation distance'
set ylabel 'compatible dependencies'

set y2label 'total mutants'

set y2tics 

set key off

set datafile separator ","

plot 'reports/ringneck/dependency-mutation-age-categorisation.csv' using 1:4 with lines, '' using 1:3 with lines axes x1y2;
