package uk.ac.glasgow.ringneck.experiment.results.latex;

import java.util.List;

import uk.ac.glasgow.ringneck.experiment.results.csv.OptimalMonoVariants;

public class OptimalMonoVariantsLaTeXTabular extends AbstractLaTeXFragment implements LaTeXFragment {
	
	final OptimalMonoVariants optimalMonoVariants;
	
	public OptimalMonoVariantsLaTeXTabular(
		OptimalMonoVariants optimalMonoVariants) {
		super (optimalMonoVariants.getLabel());
		this.optimalMonoVariants = optimalMonoVariants;
	}
	
	@Override
	public String getLaTeXContent() {
		StringBuffer result = new StringBuffer ();
		
		result.append("\\begin{tabular}{|*{5}{l|}}\\hline%\n");
		result.append("\\bf Group  & \\bf Artifact & \\bf Recommended & \\bf Permitted & \\bf Calculated \\\\ \\hline%\n");

		List<String> rowLabels = optimalMonoVariants.getRowLabels();
		
		for (String rowLabel : rowLabels){
			result.append(optimalMonoVariants.getCellData("groupid", rowLabel)).append("&");
			result.append(optimalMonoVariants.getCellData("artifactid", rowLabel)).append("&");
			result.append(optimalMonoVariants.getCellData("version", rowLabel)).append("&");
			result.append(optimalMonoVariants.getCellData("knownVersionRange", rowLabel)).append("&");
			result.append(optimalMonoVariants.getCellData("optimalVersionRange", rowLabel)).append("\\\\%\n");

		}

		result.append("\\hline\n\\end{tabular}");
		
		return result.toString();
	}


}
