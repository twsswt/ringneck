package uk.ac.glasgow.ringneck.experiments._0001;

import static java.lang.String.format;
import static org.apache.log4j.PropertyConfigurator.configure;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalReportDBFilePath;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.csv.DependencyMutantAgeCategorisation;
import uk.ac.glasgow.ringneck.experiment.results.csv.DependencyMutantBuildCounts;
import uk.ac.glasgow.ringneck.experiment.results.csv.OptimalMonoVariants;
import uk.ac.glasgow.ringneck.experiment.results.csv.ProjectDependencyCategorisation;
import uk.ac.glasgow.ringneck.experiment.results.csv.ProjectDependencyCategoryCounts;
import uk.ac.glasgow.ringneck.experiment.results.csv.ProjectMutantBuildCounts;
import uk.ac.glasgow.ringneck.experiment.results.latex.DependencyMutantBuildCountsLaTeXTabular;
import uk.ac.glasgow.ringneck.experiment.results.latex.LaTeXFragment;
import uk.ac.glasgow.ringneck.experiment.results.latex.OptimalMonoVariantsLaTeXTabular;
import uk.ac.glasgow.ringneck.experiment.results.latex.ProjectDependencyCategoryCountsLaTeXTabular;
import uk.ac.glasgow.ringneck.experiment.results.latex.ProjectMutantBuildCountsLaTeXTabular;
import uk.ac.glasgow.ringneck.experiment.results.latex.ProportionLaTeXFragment;
import uk.ac.glasgow.ringneck.experiment.results.latex.SingleCellLaTeX;

public class Results {
	
	private static final Logger logger = Logger.getLogger(Results.class);
		
	public static void main (String[] args){
		
		configure("src/main/resource/log4j.properties");
		logger.info("Beginning preparation of LaTeX inputs.");
				
		String connectionString = "jdbc:sqlite:"+getExperimentalReportDBFilePath();
		
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(connectionString);				
			
			LaTeXFragment optimalMonoVariantsLaTeXTabular =
				new OptimalMonoVariantsLaTeXTabular(
					new OptimalMonoVariants (connection));
			optimalMonoVariantsLaTeXTabular.outputLaTeXFile();
			
			ProjectMutantBuildCounts projectMutantBuildCounts = 
				new ProjectMutantBuildCounts(connection);
			
			LaTeXFragment projectMutantBuildCountsLaTeXTabular =
				new ProjectMutantBuildCountsLaTeXTabular (
					projectMutantBuildCounts);
			projectMutantBuildCountsLaTeXTabular.outputLaTeXFile();
			
			LaTeXFragment totalMutantsGenerated = 
				new SingleCellLaTeX<String,String,Object> (
					projectMutantBuildCounts, "TotalMutantsGenerated", "mutantsGenerated", "Total");
			totalMutantsGenerated.outputLaTeXFile();
			
			LaTeXFragment totalShouldHavePassedSuccess = 
				new SingleCellLaTeX<String,String,Object> (projectMutantBuildCounts,
					"TotalShouldHavePassedSuccess", "shouldHavePassedsuccess", "Total");
			totalShouldHavePassedSuccess.outputLaTeXFile();
			
			LaTeXFragment shouldHavePassedSuccessProportionOfGenerated =
				new ProportionLaTeXFragment("ShouldHavePassedSuccessProportionOfGenerated", 
					totalShouldHavePassedSuccess.getLaTeXContent(), totalMutantsGenerated.getLaTeXContent());
			
			shouldHavePassedSuccessProportionOfGenerated.outputLaTeXFile();
			
			LaTeXFragment totalShouldHavePassedSuccessMavenSettings = 
				new SingleCellLaTeX<String,String,Object> (projectMutantBuildCounts,
					"ShouldHavePassedSuccessMavenSettings", "shouldHavePassedsuccess", "org.apache:maven-settings:3.3.3");
			totalShouldHavePassedSuccessMavenSettings.outputLaTeXFile();

			LaTeXFragment totalMutantsGeneratedMavenSettings = 
				new SingleCellLaTeX<String,String,Object> (projectMutantBuildCounts,
					"TotalMutantsGeneratedMavenSettings", "mutantsGenerated", "org.apache:maven-settings:3.3.3");
			totalMutantsGeneratedMavenSettings.outputLaTeXFile();

			LaTeXFragment shouldHavePassedSuccessProportionOfGeneratedMavenSettings =
				new ProportionLaTeXFragment("ShouldHavePassedSuccessProportionOfGeneratedMavenSettings", 
					totalShouldHavePassedSuccessMavenSettings.getLaTeXContent(),
					totalMutantsGeneratedMavenSettings.getLaTeXContent());
			shouldHavePassedSuccessProportionOfGeneratedMavenSettings.outputLaTeXFile();

			LaTeXFragment totalShouldHavePassedSuccessMavenArtifact = 
				new SingleCellLaTeX<String, String, Object> (projectMutantBuildCounts,
					"ShouldHavePassedSuccessMavenArtifact", "shouldHavePassedsuccess", "org.apache:maven-artifact:3.3.3");
			totalShouldHavePassedSuccessMavenArtifact.outputLaTeXFile();

			LaTeXFragment totalMutantsGeneratedMavenArtifact = 
				new SingleCellLaTeX<String, String, Object> (projectMutantBuildCounts,
					"TotalMutantsGeneratedMavenArtifact", "mutantsGenerated", "org.apache:maven-artifact:3.3.3");
			totalMutantsGeneratedMavenArtifact.outputLaTeXFile();

			LaTeXFragment shouldHavePassedSuccessProportionOfGeneratedMavenArtifact =
				new ProportionLaTeXFragment("ShouldHavePassedSuccessProportionOfGeneratedMavenArtifact", 
					totalShouldHavePassedSuccessMavenArtifact.getLaTeXContent(),
					totalMutantsGeneratedMavenArtifact.getLaTeXContent());
			shouldHavePassedSuccessProportionOfGeneratedMavenArtifact.outputLaTeXFile();
			
			LaTeXFragment totalShouldHavePassedFailedCompile = 
				new SingleCellLaTeX<String, String, Object> (projectMutantBuildCounts,
					"TotalShouldHavePassedfailedcompile", "shouldHavePassedfailedcompile", "Total");
			totalShouldHavePassedFailedCompile.outputLaTeXFile();

			LaTeXFragment totalShouldHavePassedFailedTest = 
				new SingleCellLaTeX<String, String, Object> (projectMutantBuildCounts,
					"TotalShouldHavePassedfailedtest", "shouldHavePassedfailedtest", "Total");
			totalShouldHavePassedFailedTest.outputLaTeXFile();

			LaTeXFragment shouldHavePassedfailedtest =
				new ProportionLaTeXFragment(
					"ShouldHavePassedFailedTestProportionOfGeneratedMavenArtifact", 
					totalShouldHavePassedFailedTest.getLaTeXContent(),
					totalMutantsGenerated.getLaTeXContent());
			shouldHavePassedfailedtest.outputLaTeXFile();
			
			ProjectDependencyCategorisation projectDependencyCategorisation =
				new ProjectDependencyCategorisation (connection);
			
			ProjectDependencyCategoryCounts projectDependencyCategoryCounts = 
				new ProjectDependencyCategoryCounts(projectDependencyCategorisation);
			
			LaTeXFragment projectDependencyCategoryCountsLaTeXTabular =
			new ProjectDependencyCategoryCountsLaTeXTabular (
				projectDependencyCategoryCounts);
			projectDependencyCategoryCountsLaTeXTabular.outputLaTeXFile();

			LaTeXFragment totalDependencies = 
				new SingleCellLaTeX<String, String, Object> (projectDependencyCategoryCounts,
					"TotalDependencies", "numdependencies", "Total");
			totalDependencies.outputLaTeXFile();
			
			LaTeXFragment totalunderConstrainedDependencies = 
				new SingleCellLaTeX<String, String, Object> (projectDependencyCategoryCounts,
					"TotalUnderConstrainedDependencies", "under", "Total");
			totalunderConstrainedDependencies.outputLaTeXFile();
			
			LaTeXFragment underConstrainedDependenciesProportionOfTotal =
				new ProportionLaTeXFragment("UnderConstrainedDependenciesProportionOfTotal", 
					totalunderConstrainedDependencies.getLaTeXContent(),
					totalDependencies.getLaTeXContent());
			underConstrainedDependenciesProportionOfTotal.outputLaTeXFile();
			
			
			DependencyMutantBuildCounts dependencyMutantBuildCounts = 
				new  DependencyMutantBuildCounts(connection);
			
			LaTeXFragment dependencyMutantBuildCountsLaTeXTabular = 
				new DependencyMutantBuildCountsLaTeXTabular(
					dependencyMutantBuildCounts);
			dependencyMutantBuildCountsLaTeXTabular.outputLaTeXFile();

			LaTeXFragment mavenAetherProviderResilience = 
				new SingleCellLaTeX<String, DependencyDataLabel, Object> (dependencyMutantBuildCounts,
					"MavenAetherProviderResilience", "resillience", 
					new DependencyDataLabel("org.apache.maven","maven-aether-provider"));
			mavenAetherProviderResilience.outputLaTeXFile();
			
						
			DependencyMutantAgeCategorisation dependencyMutantAgeCategorisation =
				new DependencyMutantAgeCategorisation(
					connection);
			
			dependencyMutantAgeCategorisation.outputDataToCSVFile();
			
			
			logger.info("Finished preparation of LaTeX inputs.");

								
		} catch (Exception e) {
			logger.error(format("While creating connection [%s]", connectionString), e);
		} finally {
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(format("While creating connection [%s]", connectionString), e);
				}			
		}		 
	}
	
}
