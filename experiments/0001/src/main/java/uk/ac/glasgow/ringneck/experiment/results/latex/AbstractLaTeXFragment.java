package uk.ac.glasgow.ringneck.experiment.results.latex;

import static java.lang.String.format;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalBuildReportFilePath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.apache.log4j.Logger;

public abstract class AbstractLaTeXFragment implements LaTeXFragment{

	private static final Logger logger = Logger.getLogger(OptimalMonoVariantsLaTeXTabular.class);

	private final String label;
	
	public AbstractLaTeXFragment(String label) {
		this.label = label;
	}

	@Override
	public void outputLaTeXFile() throws FileNotFoundException {
		String reportsDirPath = getExperimentalBuildReportFilePath();
		File file = new File(format("%s/%s.tex", reportsDirPath, label));
		
		logger.warn(format("Outputting data to LaTeX file [%s].", file));
		
		FileOutputStream fis = new FileOutputStream (file);
		PrintStream printStream = new PrintStream(fis);
		printStream.print(getLaTeXContent());
		printStream.print("%");
		printStream.close();
	}

}