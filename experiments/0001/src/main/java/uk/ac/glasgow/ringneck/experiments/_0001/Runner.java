package uk.ac.glasgow.ringneck.experiments._0001;

import static java.util.Arrays.asList;
import static org.apache.log4j.PropertyConfigurator.configure;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalReportDBFilePath;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalBuildReportFilePath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import uk.ac.glasgow.ringneck.RingneckReport;
import uk.ac.glasgow.ringneck.SQLiteDatabaseOfReportsWriter;
import uk.ac.glasgow.ringneck.experiment.SubExperiment;
import uk.ac.glasgow.ringneck.experiment.SubExperimentBuilder;


@RunWith(Parameterized.class)
public class Runner {	
			
	private SubExperiment subExperiment;
	private RingneckReport report;
		
	@BeforeClass
	public static void setup (){
		configure("src/main/resource/log4j.properties");
		
		File databaseFile = new File(getExperimentalReportDBFilePath());
		databaseFile.delete();
		
		File reportDir = new File(getExperimentalBuildReportFilePath());
		reportDir.mkdirs();
	}
	
	/* Experimental parameters */
	
	@Parameterized.Parameters(name="[{0}]")
	public static List<? extends Object[]> data () throws Exception {
				
		SubExperimentBuilder subExperimentBuilder =
			new SubExperimentBuilder()
			.setGroupId("org.apache")
			.setVersion("3.3.3")
			.setScmConnectionString("git://github.com/apache/maven/")
			.setScmTag("maven-3.3.3")
			.setProjectWorkingDirectoryPath("org.apache/maven")
			.setDefaultTimeout((1000l * 60l) * 10l)
			.setMutantTimeoutFactor(3);
		
		SubExperiment [][] data = {
			{subExperimentBuilder.setArtifactId("maven").setProjectDirPath("org.apache/maven/apache-maven").build()},
			{subExperimentBuilder.setArtifactId("maven-aether-provider").setProjectDirPath("org.apache/maven/maven-aether-provider").build()},
			{subExperimentBuilder.setArtifactId("maven-artifact").setProjectDirPath("org.apache/maven/maven-artifact").build()},
			{subExperimentBuilder.setArtifactId("maven-builder-support").setProjectDirPath("org.apache/maven/maven-builder-support").build()},
			{subExperimentBuilder.setArtifactId("maven-compat").setProjectDirPath("org.apache/maven/maven-compat").build()},
			{subExperimentBuilder.setArtifactId("maven-core").setProjectDirPath("org.apache/maven/maven-core").build()},
			{subExperimentBuilder.setArtifactId("maven-embedder").setProjectDirPath("org.apache/maven/maven-embedder").build()},
			{subExperimentBuilder.setArtifactId("maven-model").setProjectDirPath("org.apache/maven/maven-model").build()},
			{subExperimentBuilder.setArtifactId("maven-model-builder").setProjectDirPath("org.apache/maven/maven-model-builder").build()},
			{subExperimentBuilder.setArtifactId("maven-plugin-api").setProjectDirPath("org.apache/maven/maven-plugin-api").build()},
			{subExperimentBuilder.setArtifactId("maven-repository-metadata").setProjectDirPath("org.apache/maven/maven-repository-data").build()},
			{subExperimentBuilder.setArtifactId("maven-settings").setProjectDirPath("org.apache/maven/maven-settings").build()},
			{subExperimentBuilder.setArtifactId("maven-settings-builder").setProjectDirPath("org.apache/maven/maven-settings-builder").build()}
 		};
		
		return asList(data);		
	}
		
	public Runner (SubExperiment subExperiment) throws IOException, XmlPullParserException {
		this.subExperiment = subExperiment;
	}
						
	@Test
	public void runExperiment ()  {
		report = subExperiment.runExperiment();
	}
	
	@After
	public void tearDown (){
		
		subExperiment = null;
		
		File databaseFile = new File(getExperimentalReportDBFilePath());
		
		SQLiteDatabaseOfReportsWriter databaseOfReportsWriter = 
			new SQLiteDatabaseOfReportsWriter(databaseFile);
		
		databaseOfReportsWriter.processExperiment(report);
		
		report = null;
	}

}
