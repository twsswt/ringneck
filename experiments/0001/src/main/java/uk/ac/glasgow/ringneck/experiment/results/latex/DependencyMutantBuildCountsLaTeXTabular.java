package uk.ac.glasgow.ringneck.experiment.results.latex;

import java.util.List;

import uk.ac.glasgow.ringneck.experiment.results.DependencyDataLabel;
import uk.ac.glasgow.ringneck.experiment.results.csv.DependencyMutantBuildCounts;

public class DependencyMutantBuildCountsLaTeXTabular
	extends AbstractLaTeXFragment implements LaTeXFragment {

	private final DependencyMutantBuildCounts dependencyMutantBuildCounts;
	
	public DependencyMutantBuildCountsLaTeXTabular(
		DependencyMutantBuildCounts dependencyMutantBuildCounts) {
			super(dependencyMutantBuildCounts.getLabel());
			
			this.dependencyMutantBuildCounts = dependencyMutantBuildCounts;
	}

	@Override
	public String getLaTeXContent() {
		StringBuffer result = new StringBuffer ();
		
	      
		
		result.append("\\begin{tabular}{|l|r|r|r|r|r|r|r|}\\hline%\n");
		result.append("\\bf Dependency & \\bf Ver & \\bf S & \\bf FT & \\bf FC & \\bf U & \\bf Tot& \\bf Res\\\\ \\hline%\n");

		List<DependencyDataLabel> rowLabels = dependencyMutantBuildCounts.getRowLabels();
		
		for (DependencyDataLabel rowLabel : rowLabels){
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("artifact", rowLabel, "0")).append("&");
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("numversions", rowLabel, "0")).append("&");
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("success", rowLabel, "0")).append("&");
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("failedtest", rowLabel, "0")).append("&");
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("failedcompile", rowLabel, "0")).append("&");
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("timedoutunknown", rowLabel, "0")).append("&");
			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("totalmutants", rowLabel, "0")).append("&");

			result.append(dependencyMutantBuildCounts.getCellDataOrDefault("resillience", rowLabel, "0")).append("\\\\%\n");
		}
		
		result.append("\\hline\n\\end{tabular}");
		
		return result.toString();

	}

}
