package uk.ac.glasgow.ringneck.experiment.results.latex;

import java.util.List;

import uk.ac.glasgow.ringneck.experiment.results.csv.ProjectMutantBuildCounts;

public class ProjectMutantBuildCountsLaTeXTabular
	extends AbstractLaTeXFragment {

	private final ProjectMutantBuildCounts projectMutantBuildCounts;

	public ProjectMutantBuildCountsLaTeXTabular(ProjectMutantBuildCounts projectMutantBuildCounts) {
		super(projectMutantBuildCounts.getLabel());
		this.projectMutantBuildCounts = projectMutantBuildCounts;
	}

	@Override
	public String getLaTeXContent() {
		StringBuffer result = new StringBuffer ();
		
		result.append("\\begin{tabular}{|l|r|r|r|r|r|}\\hline%\n");
		result.append("\\bfseries Sub-project & \\bf Mut & \\bf S & \\bf FT & \\bf FC & \\bf U \\\\\\hline%\n");

		List<String> rowLabels = projectMutantBuildCounts.getRowLabels();
		rowLabels.remove("ProportionOfGenerated");
		rowLabels.remove("Total");
		
		for (String rowLabel : rowLabels){
			result.append(projectMutantBuildCounts.getCellDataOrDefault("artifact", rowLabel, "0")).append("&");
			result.append(projectMutantBuildCounts.getCellDataOrDefault("mutantsGenerated", rowLabel, "0")).append("&");
			result.append(projectMutantBuildCounts.getCellDataOrDefault("shouldHavePassedsuccess", rowLabel, "0")).append("&");
			result.append(projectMutantBuildCounts.getCellDataOrDefault("shouldHavePassedfailedtest", rowLabel, "0")).append("&");
			result.append(projectMutantBuildCounts.getCellDataOrDefault("shouldHavePassedfailedcompile", rowLabel, "0")).append("&");
			result.append(projectMutantBuildCounts.getCellDataOrDefault("shouldHavePassedunknown", rowLabel, "0")).append("\\\\%\n");
		}
		
		result.append(" Total &");
		result.append(projectMutantBuildCounts.getCellData("mutantsGenerated", "Total")).append("&");
		result.append(projectMutantBuildCounts.getCellData("shouldHavePassedsuccess", "Total")).append("&");
		result.append(projectMutantBuildCounts.getCellData("shouldHavePassedfailedtest", "Total")).append("&");
		result.append(projectMutantBuildCounts.getCellData("shouldHavePassedfailedcompile", "Total")).append("&");
		result.append(projectMutantBuildCounts.getCellData("shouldHavePassedunknown", "Total")).append("\\\\%\n");
		
		
		result.append("\\hline\n\\end{tabular}");
		
		return result.toString();
	}

}
