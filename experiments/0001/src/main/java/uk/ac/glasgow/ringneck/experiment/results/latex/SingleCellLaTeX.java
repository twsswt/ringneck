package uk.ac.glasgow.ringneck.experiment.results.latex;

import uk.ac.glasgow.ringneck.experiment.results.DataTable;

public class SingleCellLaTeX<C,R,D>
	extends AbstractLaTeXFragment implements LaTeXFragment {

	private final DataTable<C,R,D> dataTable;
	
	private final C columnLabel;
	private final R rowLabel;
		
	public SingleCellLaTeX(
		DataTable<C,R,D> dataTable, String filename, C columnLabel, R rowLabel) {
		super(filename);
		this.dataTable = dataTable;
		this.columnLabel = columnLabel;
		this.rowLabel = rowLabel;
	}

	@Override
	public String getLaTeXContent() {
		return dataTable.getCellData(columnLabel, rowLabel).toString();
	}

}
