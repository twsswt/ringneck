package uk.ac.glasgow.ringneck.experiment.results.latex;

import static java.lang.Integer.parseInt;

public class ProportionLaTeXFragment extends AbstractLaTeXFragment {
	
	private final Integer quotient;
	private final Integer divisor;
	
	public ProportionLaTeXFragment(String label, String quotient, String divisor) {
		super(label);
		this.quotient = parseInt(quotient);
		this.divisor = parseInt(divisor);
	}

	@Override
	public String getLaTeXContent() {
		
		Double result = quotient * 100.0 / divisor;
		
		return String.format("%.0f", result);
			
	}
}