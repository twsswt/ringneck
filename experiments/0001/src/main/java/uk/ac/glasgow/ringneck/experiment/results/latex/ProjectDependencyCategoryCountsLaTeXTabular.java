package uk.ac.glasgow.ringneck.experiment.results.latex;

import java.util.List;

import uk.ac.glasgow.ringneck.experiment.results.csv.ProjectDependencyCategoryCounts;

public class ProjectDependencyCategoryCountsLaTeXTabular
	extends AbstractLaTeXFragment implements LaTeXFragment {

	private final ProjectDependencyCategoryCounts projectDependencyCategoryCounts;

	public ProjectDependencyCategoryCountsLaTeXTabular(
		ProjectDependencyCategoryCounts projectDependencyCategoryCounts) {
		super(projectDependencyCategoryCounts.getLabel());
		
		this.projectDependencyCategoryCounts = projectDependencyCategoryCounts;
	}

	@Override
	public String getLaTeXContent() {
		StringBuffer result = new StringBuffer ();
		
		result.append("\\begin{tabular}{|l|r|r|r|r|}\\hline%\n");
		result.append("\\bfseries Sub-project & \\bf C & \\bf U & \\bf Un & \\bf Tot \\\\ \\hline%\n");

		List<String> rowLabels = projectDependencyCategoryCounts.getRowLabels();
		rowLabels.remove("Total");
		
		for (String rowLabel : rowLabels){
			result.append(projectDependencyCategoryCounts.getCellDataOrDefault("artifactid", rowLabel, "0")).append("&");
			result.append(projectDependencyCategoryCounts.getCellDataOrDefault("correct", rowLabel, "0")).append("&");
			result.append(projectDependencyCategoryCounts.getCellDataOrDefault("under", rowLabel, "0")).append("&");
			result.append(projectDependencyCategoryCounts.getCellDataOrDefault("untested", rowLabel, "0")).append("&");
			result.append(projectDependencyCategoryCounts.getCellDataOrDefault("numdependencies", rowLabel, "0")).append("\\\\%\n");
		}
		
		result.append(" Total &");
		result.append(projectDependencyCategoryCounts.getCellData("correct", "Total")).append("&");
		result.append(projectDependencyCategoryCounts.getCellData("under", "Total")).append("&");
		result.append(projectDependencyCategoryCounts.getCellData("untested", "Total")).append("&");
		result.append(projectDependencyCategoryCounts.getCellData("numdependencies", "Total")).append("\\\\ %\n");
		
		
		result.append("\\hline\n\\end{tabular}");
		
		return result.toString();
	}

}
