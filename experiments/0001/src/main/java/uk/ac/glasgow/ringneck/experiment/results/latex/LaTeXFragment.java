package uk.ac.glasgow.ringneck.experiment.results.latex;

import java.io.FileNotFoundException;

public interface LaTeXFragment {

	public void outputLaTeXFile() throws FileNotFoundException;
		
	public String getLaTeXContent();

}