#!/usr/bin/bash

## Summary
## ==
## Produces a summary CSV file of characteristics for target projects.
## Author: tws
##
## The script is currently hard coded to work only with experiment 0001.

## Input Parameters
## ==

TARGET_PROJECT=org.apache/maven

SUB_PROJECTS=( apache-maven
	       maven-aether-provider 
	       maven-artifact
	       maven-builder-support
	       maven-compat
	       maven-core
	       maven-embedder
	       maven-model
	       maven-model-builder
	       maven-plugin-api
	       maven-settings
	       maven-settings-builder );

## End of input parameters.

OUTPUT_FILE=reports/ringneck/project-summary.tex

rm -f $OUTPUT_FILE

echo "\begin{tabular}{|l|r|r|r|}\hline" > $OUTPUT_FILE
echo "\bf project & \bf LoC & \bf Dep & \bf PIT\% \\\\ \hline" >> $OUTPUT_FILE

for SUB_PROJECT in ${SUB_PROJECTS[@]}; do

TARGET_PROJECT_DIR=target/workspaces/$TARGET_PROJECT/$SUB_PROJECT

RINGNECK_HOME=`pwd`

cd $TARGET_PROJECT_DIR

LOC=`find ./src/main/java -name "*.java" | xargs cat | wc -l`

DEPENDENCIES=`grep "<dependency>" pom.xml | wc -l`

cd $RINGNECK_HOME

MUTANT_REPORT="$TARGET_PROJECT_DIR/target/pit-reports/mutations.csv"

if [ -f $MUTANT_REPORT ];
then

    echo "Found Pitest for $SUB_PROJECT"
    
    TOTAL_MUTANTS=`cat $MUTANT_REPORT | wc -l`

    TOTAL_KILLED=`grep KILLED $MUTANT_REPORT | wc -l`

    PERCENT_MUTANTS=`echo "scale=0; 100 * $TOTAL_KILLED/$TOTAL_MUTANTS" | bc`

else
    PERCENT_MUTANTS=0
fi;     

echo "$SUB_PROJECT & $LOC & $DEPENDENCIES & $PERCENT_MUTANTS \\\\" >> $OUTPUT_FILE

done;


echo "\hline" >> $OUTPUT_FILE
echo "\end{tabular}" >> $OUTPUT_FILE
