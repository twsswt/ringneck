#!/usr/bin/bash

## Summary
## ==
## Executes pitest on a set of target projects.
## Author: tws
##
## The script is currently hard coded to work only with experiment 0001.

## Input Parameters
## ==

SCM_CONNECTION_STRING=git://github.com/apache/maven/

SCM_TAG="maven-3.3.3"

TARGET_PROJECT=org.apache/maven

SUB_PROJECTS=( apache-maven
	       maven-aether-provider 
	       maven-artifact
	       maven-builder-support
	       maven-compat
	       maven-core
	       maven-embedder
	       maven-model
	       maven-model-builder
	       maven-plugin-api
	       maven-settings
	       maven-settings-builder );

TARGET_CLASSES=org.apache.maven.*
EXCLUDED_CLASSES=org.apache.maven.execution.*
TARGET_TESTS=org.apache.maven.*

## End of input parameters.

WORKING_COPY_DIR=target/workspaces/$TARGET_PROJECT

if [ ! -d $WORKING_COPY_DIR ]; then
	echo Cloning $SCM_CONNECTION_STRING to $WORKING_COPY_DIR
	mkdir -p $WORKING_COPY_DIR
	git clone $SCM_CONNECTION_STRING $WORKING_COPY_DIR
else
	git --work-tree=$WORKING_COPY_DIR --git-dir=$WORKING_COPY_DIR/.git reset --hard
fi

 git --work-tree=$WORKING_COPY_DIR --git-dir=$WORKING_COPY_DIR/.git checkout $SCM_TAG

for SUB_PROJECT in ${SUB_PROJECTS[@]}; do

TARGET_PROJECT_DIR=$WORKING_COPY_DIR/$SUB_PROJECT

RINGNECK_HOME=`pwd`

cd $TARGET_PROJECT_DIR

mvn test

mvn org.pitest:pitest-maven:1.0.0:mutationCoverage \
 -DtimestampedReports=false \
 -DoutputFormats=csv \
 -DtargetClasses=${TARGET_CLASSES} \
 -DexcludedClasses=${EXCLUDED_CLASSES}\
 -DtargetTests=${TARGET_TESTS}

cd $RINGNECK_HOME

done;
