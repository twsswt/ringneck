package uk.ac.glasgow.ringneck.experiments._0002;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.ProjectPopulationFromMavenJSONQuery;
import uk.ac.glasgow.ringneck.RingneckException;
import uk.ac.glasgow.ringneck.scm.ScmDeployerFactory;

/**
 * Validates the expected characteristics of a large population of candidate projects.
 * @author tws
 *
 */
public class Experiment0002ProjectPopulationCharacteristicsCheck {

	/**
	 * References a file containing the content of the following URL cached 2014/12/04.
	 * http://search.maven.org/solrsearch/select?q=p:"pom"&rows=20000&wt=json
	 */
	private static final String jSONQueryResultFileName = 
		"src/main/resource/cached.json";

	protected static ProjectPopulationFromMavenJSONQuery projectPopulation;

	private static Map<String,Integer> scmTypeCount;
	
	@BeforeClass
	public static void setUp () throws Exception {
		PropertyConfigurator.configure("src/main/resource/log4j.properties");
		
		File jSONQueryResultFile = 
			new File(jSONQueryResultFileName);
		
		projectPopulation =
			new ProjectPopulationFromMavenJSONQuery(jSONQueryResultFile);
		
		collateScmStatistics();
	}

	private static void collateScmStatistics() throws Exception {
		scmTypeCount = new HashMap<String,Integer>();
		
		for (ModelHelper modelHelper: projectPopulation){
			if (modelHelper != null){
								
				try {
					String formattedScmConnection = 
						modelHelper.getScmConnectionString();
					
	
					String scmType = (formattedScmConnection != null)?
						ScmDeployerFactory.determineSCMConnectionStringType(formattedScmConnection)
						:null;
				
						Integer count = scmTypeCount.get(scmType);
						if (count == null) count = 0;
						count++;
				
						scmTypeCount.put(scmType, count);
				
				} catch (RingneckException e){
					//skip
				}

			}
		}
	}
	
	@Test
	public void testGitSCMCount () throws Exception {			
		testSCMTypeCount("git", 8259);
	}
	
	@Test
	public void testSubversionSCMCount () throws Exception {			
		testSCMTypeCount("svn", 4610);

	}
	
	@Test
	public void testMercurialSCMCount () throws Exception {			
		testSCMTypeCount("hg", 197);
	}

	private void testSCMTypeCount(String scmType, Integer expected) {
		Integer actual = scmTypeCount.get(scmType);
		
		String message = 
			format(
				"Expected number of projects with [%s] SCM connection strings.",scmType);
				
		assertEquals(message, expected, actual);
	}
	
	@Test
	public void testTotalSCMTypeCount(){
		
		Integer  actual = 
			scmTypeCount.entrySet().stream()
				.filter(e -> e.getKey() != null)
				.map(Map.Entry::getValue)
				.reduce(0, (a, b) -> a + b);
		
		Integer expected = 12887;

		String message =
			"Expected number of projects with SCM connection strings.";
						
		assertEquals(message, expected, actual);
	}
	
	@Test
	public void testProjectCount() throws Exception {
		
		Integer expected = 13948;
		Integer actual = projectPopulation.size();

		String message = 
			"Expected number of projects in query.";	
		
		assertEquals(message, expected, actual);		
	}

}
