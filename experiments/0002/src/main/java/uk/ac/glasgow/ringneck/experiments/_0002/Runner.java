package uk.ac.glasgow.ringneck.experiments._0002;

import static java.lang.String.format;
import static org.apache.log4j.PropertyConfigurator.configure;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalBuildReportFilePath;
import static uk.ac.glasgow.ringneck.experiment.ExperimentalConstants.getExperimentalReportDBFilePath;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import uk.ac.glasgow.ringneck.ModelHelper;
import uk.ac.glasgow.ringneck.ProjectPopulationFromMavenJSONQuery;
import uk.ac.glasgow.ringneck.RingneckException;
import uk.ac.glasgow.ringneck.RingneckReport;
import uk.ac.glasgow.ringneck.SQLiteDatabaseOfReportsWriter;
import uk.ac.glasgow.ringneck.experiment.SubExperiment;
import uk.ac.glasgow.ringneck.experiment.SubExperimentBuilder;

@RunWith(Parameterized.class)
public class Runner {
	
	private static final Logger logger = Logger.getLogger(Runner.class);
	
	@BeforeClass
	public static void beforeClass () {

		logger.info("Beginning experiment.");
		
		File databaseFile = new File(getExperimentalReportDBFilePath());
		databaseFile.delete();
		
		File reportDir = new File(getExperimentalBuildReportFilePath());
		reportDir.mkdirs();

	}
			
	@Parameters
	public static List<Object[]> data () throws Exception {
		
		configure("src/main/resource/log4j.properties");
		
		List<Object[]> result = new ArrayList<Object[]>();
		
		String cachedJsonQueryResultsFilePath = 
			"src/main/resource/cached.json";
				
		logger.info(
			format(
				"Reading experimental projects from cached query file [%s].",
				cachedJsonQueryResultsFilePath));
		
		File cachedJSONQueryResultFile = 
			new File(cachedJsonQueryResultsFilePath);
		
		ProjectPopulationFromMavenJSONQuery projectPopulation =
			new ProjectPopulationFromMavenJSONQuery(cachedJSONQueryResultFile);

		for (ModelHelper modelHelper : projectPopulation){
			
			if (modelHelper == null) continue;


			String projectName = modelHelper.getProjectName();
			logger.info(format("Checking properties of project found in search [%s].", projectName));
			
			try {
				String scmConnectionString = modelHelper.getScmConnectionString();
				String scmTag = modelHelper.getScmTag();

				
				if (scmConnectionString == null){
					logger.info(format("Project [%s] has no Scm connection string, skipping.", projectName));
					continue;
				}
				
				else if (scmTag == null){
					logger.info(format("Project [%s] has no Scm tag, skipping.", projectName));
					continue;
				} else {
					logger.info(format("Adding project [%s] for analysis.", projectName));
					result.add(new Object[]{modelHelper});
				}	
			
			} catch (RingneckException e){
				logger.warn(format("Couldn't resolve Scm details for project [%s].", projectName),e);
			}
			
		}
		logger.info(format("Found [%d] projects for analysis.",result.size()));
		return result;
	}
	
	private SubExperiment subExperiment;
	private RingneckReport report;
	
	public Runner (ModelHelper modelHelper) throws Exception {
		
		String groupId = modelHelper.getProjectGroupId();
		String artifactId = modelHelper.getModel().getArtifactId();
		String version = modelHelper.getModelVersion();
		String scmConnectionString = modelHelper.getScmConnectionString();
		String scmTag = modelHelper.getScmTag();
		String workingCopyDirectory = modelHelper.getWorkingDirectory(new File(".")).getPath();

		
		this.subExperiment =
			new SubExperimentBuilder()
				.setGroupId(groupId)
				.setArtifactId(artifactId)
				.setVersion(version)
				.setScmConnectionString(scmConnectionString)
				.setScmTag(scmTag)
				.setProjectWorkingDirectoryPath(workingCopyDirectory)
				.setProjectDirPath(workingCopyDirectory)
				.setDefaultTimeout(1000l * 60l * 10l)
				.setMutantTimeoutFactor(10)
				.build();
	}
	
	@Test
	public void test (){
		report = subExperiment.runExperiment();		
	}
	
	@After
	public void tearDown (){
		
		subExperiment = null;
		
		SQLiteDatabaseOfReportsWriter databaseOfReportsWriter = 
			new SQLiteDatabaseOfReportsWriter(
				new File(getExperimentalReportDBFilePath()));
		
		databaseOfReportsWriter.processExperiment(report);
		
		report = null;
		
		File workingCopyDirectory = new File (subExperiment.workingCopyDirectoryPath);
		removeDirectory(workingCopyDirectory);
	}
	
	private void removeDirectory(File file){
		if (file.isDirectory())
			for (File entry : file.listFiles())
				removeDirectory(entry);
		
		file.delete();
	}
	
}
